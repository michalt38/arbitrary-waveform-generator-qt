#-------------------------------------------------
#
# Project created by QtCreator 2016-09-08T10:48:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Arbitrary-waveform-generator
TEMPLATE = app


SOURCES += main.cpp\
        arbgenerator.cpp

HEADERS  += arbgenerator.h

FORMS    += arbgenerator.ui
