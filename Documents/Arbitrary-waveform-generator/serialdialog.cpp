#include "serialdialog.h"
#include "ui_serialdialog.h"
#include <QSerialPortInfo>
#include <QModelIndex>
#include <QStringList>
#include <QStandardItem>

SerialDialog::SerialDialog(ecg::Serial &serial, QWidget *parent) :
    QDialog(parent), ui(new Ui::SerialDialog), _serial(serial)
{
    ui->setupUi(this);

    QSerialPortInfo ports;
    com = ports.availablePorts().at(0).portName();
    baud_rate = 115200;

    connect(ui->com_tableView, SIGNAL(clicked(QModelIndex)), this, SLOT(set_com(QModelIndex)));
    connect(ui->baudRate_spinBox, SIGNAL(valueChanged(int)), this, SLOT(set_baud_rate(int)));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(serial_accepted()));
    connect(ui->com_tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(double_clicked(QModelIndex)));

    ui->com_tableView->setShowGrid(false);
    ui->com_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->com_tableView->verticalHeader()->hide();
    ui->com_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->com_tableView->horizontalHeader()->setHighlightSections(false);

    serial_model = new QStandardItemModel(ports.availablePorts().length(),3,this);
    serial_model->setHorizontalHeaderItem(0, new QStandardItem(QString("Port name")));
    serial_model->setHorizontalHeaderItem(1, new QStandardItem(QString("Description")));
    serial_model->setHorizontalHeaderItem(2, new QStandardItem(QString("Serial number")));
    for(int i = 0; i < ports.availablePorts().length(); i++)
    {
        serial_model->setItem(i,0, new QStandardItem(ports.availablePorts().at(i).portName()));
        serial_model->setItem(i,1, new QStandardItem(ports.availablePorts().at(i).description()));
        serial_model->setItem(i,2, new QStandardItem(ports.availablePorts().at(i).serialNumber()));
    }
    ui->com_tableView->setModel(serial_model);
    this->setWindowTitle("Serial port");
}

void SerialDialog::double_clicked(QModelIndex index)
{
    _serial.set_baud_rate(baud_rate);
    com = QSerialPortInfo::availablePorts().at(index.row()).portName();
    _serial.set_device(com.toStdString());
    this->accept();
}

void SerialDialog::refresh()
{
    delete serial_model;

    QSerialPortInfo ports;
    com = ports.availablePorts().at(0).portName();

    serial_model = new QStandardItemModel(ports.availablePorts().length(),3,this);
    serial_model->setHorizontalHeaderItem(0, new QStandardItem(QString("Port name")));
    serial_model->setHorizontalHeaderItem(1, new QStandardItem(QString("Description")));
    serial_model->setHorizontalHeaderItem(2, new QStandardItem(QString("Serial number")));
    for(int i = 0; i < ports.availablePorts().length(); i++)
    {
        serial_model->setItem(i,0, new QStandardItem(ports.availablePorts().at(i).portName()));
        serial_model->setItem(i,1, new QStandardItem(ports.availablePorts().at(i).description()));
        serial_model->setItem(i,2, new QStandardItem(ports.availablePorts().at(i).serialNumber()));
    }
    ui->com_tableView->setModel(serial_model);
}

void SerialDialog::set_baud_rate(int _baud_rate)
{
    baud_rate = _baud_rate;
}

void SerialDialog::set_com(QModelIndex index)
{
    com = QSerialPortInfo::availablePorts().at(index.row()).portName();
}

void SerialDialog::serial_accepted()
{
    _serial.set_baud_rate(baud_rate);
    _serial.set_device(com.toStdString());
}

SerialDialog::~SerialDialog()
{
    delete ui;
    delete serial_model;
}
