#ifndef ARBGENERATOR_H
#define ARBGENERATOR_H

#include <QMainWindow>

namespace Ui {
class ArbGenerator;
}

class ArbGenerator : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArbGenerator(QWidget *parent = 0);
    ~ArbGenerator();

private:
    Ui::ArbGenerator *ui;
};

#endif // ARBGENERATOR_H
