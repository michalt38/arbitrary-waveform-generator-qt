#ifndef SERIALDIALOG_H
#define SERIALDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include <QString>
#include <serial/serial.h>

namespace Ui {
class SerialDialog;
}

class SerialDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SerialDialog(ecg::Serial &serial, QWidget *parent = 0);
    void refresh();
    ~SerialDialog();

public slots:
    void set_baud_rate(int _baud_rate);
    void set_com(QModelIndex _com);
    void serial_accepted();
    void double_clicked(QModelIndex);

private:
    Ui::SerialDialog *ui;
    QStandardItemModel *serial_model;
    int baud_rate;
    QString com;
    ecg::Serial &_serial;
};

#endif // SERIALDIALOG_H
