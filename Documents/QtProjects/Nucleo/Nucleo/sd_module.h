/*
 * sd_module.h
 *
 *  Created on: 15 lip 2016
 *      Author: Mateusz R.
 */

#ifndef SD_MODULE_H_
#define SD_MODULE_H_

#include "fatfs.h"
#include "sdmmc.h"
#include "fatfs.h"
#include "dac.h"
#include "stdbool.h"

#define USE_CARD_DETECT 0            // uzyc pinu do wykrycia karty?
#define EXT_RED_LED 1				 // wlaczyc zew czerwona diode?

/*
 * Struktura do przechowywania info o plikach(nazwa, atrybuty etc.) + ich sciezka
 */
typedef struct File_Item{

	FILINFO item_list;
	char path[_MAX_LFN + 1]; //_MAX_LF N to definicja z fconf.h albo ff.h dotyczy rozmiaru nazwy pliku

}FILNFO_ITEMS;

enum error_code {NO_SD = 1, FAT_ERR, FIL_OPEN_ERR, DIR_OPEN_ERR, BAD_ALLOC, BAD_FATFS_INIT};

// dane EKG
extern 	int16_t **ecg_data;

// lista plikow wraz z szczegolami i sciezka
extern FILNFO_ITEMS* sd_files;

//liczba kolumn w pliku csv
extern uint16_t  nr_col;

//liczba kolumn w pliku csv
extern uint16_t  nr_row;

//liczba plik�w na karcie sd

extern uint8_t nr_files;

//uchwyt uart by moc wyslac na kompa dane
extern UART_HandleTypeDef huart2;

extern enum  enum_error error_code;


/*
 * Montuje dysk logiczny, sczytuje liczb� plik�w na karcie i uzupe�nia informacje o plikach do struktury FILNFO_ITEMS
 */
FRESULT SD_Module_Init(void);
/*
 * Parsuje plik CSV i wydobywa z niego dane dla DAC, przesy�aj�c je do podanej tablicy
 * Skladowa f Process_File
 */
FRESULT Parse_CSV(int16_t** data, uint16_t nr_col, char *filename);
/*
 * Odczytuje ile plik CSV posiada kolumn i wierszy zwaraca je do podanych argumentow
 * Skladowa f Process_File
 */
FRESULT Read_CSV(char *filename, uint16_t *nr_col, uint16_t *nr_row);
/*
 * Przetwarza dany plik, alokuje pamiec dla tablicy na dane DAC (ecg_data)
 * TEST: po odczytaniu danych i wyswietleniu zeruje tablice (memset)
 * TEST: Wyswietla dane na konsoli (UART) | Zwalnia pamiec tablicy gdy konczy prace
 */
void Process_File(uint8_t file_nr, FILNFO_ITEMS *items);
/*
 *  Skanuje wszystkie foldery w poszukiwaniu plikow CSV, zwraca ilosc znalezionych plikow
 *  skladowa f SD_Module_Init
 */
FRESULT Scan_Files (char* path, uint8_t* count_files);
/*
 *  Skanuje wszystkie foldery w poszukiwaniu plikow CSV,
 *  uzupelnia strukture FILNFO_ITEMS informacjami - skladowa f SD_Module_Init
 */
FRESULT List_Files (char* path, FILNFO_ITEMS* item);
/*
 * Funkcja do wypisywania rzeczy na konsole, param text = NULL wtedy wypisuje tylko dane z data
 * gdy text != NULL to param data nie jest potrzebny tak samo jak nr_row i nr_col, wpisujemy w text znaki specjalne
 */

bool UART_Create_File(const char* filename);
bool UART_Write_To_File(const char* data, const char* filename, UINT* bw);
//TEST//////////////////
void Simulate_IRQ(void);
//TEST//////////////////
void UART_Print(int16_t** data, const char* text, uint16_t nr_row, uint16_t nr_col);

#endif /* SD_MODULE_H_ */
