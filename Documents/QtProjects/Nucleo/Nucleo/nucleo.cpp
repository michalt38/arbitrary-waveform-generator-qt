#include "nucleo.h"
#include "ui_nucleo.h"

Nucleo::Nucleo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Nucleo)
{
    ui->setupUi(this);
}

Nucleo::~Nucleo()
{
    delete ui;
}
