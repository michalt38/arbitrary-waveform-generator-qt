#include "nucleo.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Nucleo w;
    w.show();

    return a.exec();
}
