#ifndef NUCLEO_H
#define NUCLEO_H

#include <QMainWindow>

namespace Ui {
class Nucleo;
}

class Nucleo : public QMainWindow
{
    Q_OBJECT

public:
    explicit Nucleo(QWidget *parent = 0);
    ~Nucleo();

private:
    Ui::Nucleo *ui;
};

#endif // NUCLEO_H
