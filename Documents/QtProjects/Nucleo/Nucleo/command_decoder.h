typedef enum KeywordCode {ON, OFF, BPM, CONNECT, AMPL, DISCONNECT} KeywordCode;
enum CompResult {DIFFERENT, EQUAL};

unsigned char ucFindTokensInString(char *);
enum Result eStringKeyword(char [], KeywordCode *);
void DecodeTokens(void);
void DecodeMsg(char *);
enum Result eToken_GetKeywordCode(unsigned char, KeywordCode*);
enum Result eToken_GetNumber(unsigned char, unsigned int*);
