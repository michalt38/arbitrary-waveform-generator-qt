/*
 * sd_module.c
 *
 *  Created on: 15 lip 2016
 *      Author: Mateusz R.
 *  Comment: By zadzia�a�a obs�uga LFN naleza�o sciagnac bezposrednio bilbioteke ze strony FatFs i podmienic w CubeMX pliki ff.h ff.c i fconf.h (cubeMX uzywa starej wersji)
 *   W fconf.h u�yc define "USE_LFN" ustawic na 1 i ustawic uzywanie funkcji operujacych na string np f_gets (tez odpoweidni define na wartosc 2) nie uzywac unicode.c i bez define unicode
 *   Dorzucic plik cc932.c do projektu
 */
#include "stm32l4xx_hal.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "sd_module.h"

//#define UART

	// dane z odczytanego pliku csv
	int16_t **ecg_data;

	//liczba kolumn ecg_data
	uint16_t  nr_col = 0;

	//liczba wierszy ecg_data
	uint16_t nr_row = 0;

	// lista plikow wraz z szczegolami i sciezka
	FILNFO_ITEMS *sd_files = NULL;

	// zliczenie plikow z funkcji scan_files
	uint8_t nr_files;

	//File system object for RAM disk logical drive
	FATFS SDFatFs;

	uint8_t error;

	_Bool ecg_data_malloc = false;

	//separator miedzy kolumanmi w pliku csv
	const char col_separ = ',';


FRESULT SD_Module_Init(void){

	FRESULT fr;

	fr = f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0);

	if(fr != FR_OK)
	    {
	      /* FatFs Initialization Error */
			error = BAD_FATFS_INIT;
			Error_Handler();
	    }

	 	 	Scan_Files("0:", &nr_files); // oblicz ile plikow

	 	 	// ----------------- ALOKACJA tablicy z strukturami plikow
				sd_files = (FILNFO_ITEMS *) malloc(nr_files * sizeof(FILNFO_ITEMS));
				if(sd_files == NULL){
					error = BAD_ALLOC;
					Error_Handler();
				}
			// ----------------
			List_Files("0:", sd_files); // pobierz i zapisz do sd_files informacje o nich
	return fr;
}

FRESULT Parse_CSV(
		int16_t** data, // tablica do przechowywania danych z pliku [OUT]
		uint16_t nr_row, // liczba wierszy macierzy data [IN]
		char *filename  // sciezka do pliku [IN]
		)
{
	FIL csv_file;
	char *record,*line;
	FRESULT fr;
	char buffer[256];
	uint16_t i=0,j=0;

	fr = f_open(&csv_file, filename, FA_READ);

    if(fr != FR_OK)
    {
    	error = FIL_OPEN_ERR;
    	Error_Handler();
    }
    line=f_gets(buffer,sizeof(buffer),&csv_file); // by pominac naglowek pliku csv

    while((line=f_gets(buffer,sizeof(buffer),&csv_file))!=NULL)
    {
      record = strtok(line, &col_separ);
      while(record != NULL)
      {
      data[i++][j] = atoi(record) ; // zamien string na int16_t
      record = strtok(NULL, &col_separ);
      if(i == nr_row) i=0;
      }
      ++j ;
    }

    f_close(&csv_file);
    return fr;
}

FRESULT Read_CSV(
		char *filename,   	// sciezka do pliku [IN]
		uint16_t *nr_col,  	// zwracana liczba kolumn [OUT]
		uint16_t *nr_row) 	// zwracana liczba wierszy [OUT]
{
	FIL csv_file;
	char *record,*line;
	FRESULT fr;
	char buffer[256];
	*nr_col = 0;
	*nr_row = 1;	// 1 bo nie ma przecinka na koncu w pliku csv

	fr = f_open(&csv_file, filename, FA_READ);

    if(fr != FR_OK)
    {
    	error = FIL_OPEN_ERR;
    	Error_Handler();
    }
    line=f_gets(buffer,sizeof(buffer),&csv_file); // by pominac naglowek pliku csv
    uint8_t a = 0;

    if(strstr(line, &col_separ) != NULL && strstr(line, "\n") != NULL){


			while(line[a] != '\n'){

				if(line[a] == col_separ){
					(*nr_row)++;
				}
				a++;
			}
			while((line=f_gets(buffer,sizeof(buffer),&csv_file))!=NULL)
			{

			  record = strtok(line, &col_separ);
			  while(record != NULL)
			  {

			  record = strtok(NULL, &col_separ);

			  }
			  (*nr_col)++;

			}
			f_close(&csv_file);
			return fr;
    }else
    	return 0xFF;
}

FRESULT List_Files (
    char* path,        /* Start node to be scanned (***also used as work area***)[IN] */
    FILNFO_ITEMS* item // struktura do ktorej beda wpisywane informacje o plikach [OUT]
)
{
    FRESULT res;
    DIR dir;
    static uint8_t i;
    uint8_t p = 0;
    static FILINFO fno;


    res = f_opendir(&dir, path);                       /* Open the directory */
    if (res == FR_OK) {
        for (;;) {
            res = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break; /* Break on error or end of dir */
				if (!strncmp(fno.fname, "System",6)){
					continue;
				}else{

					if (fno.fattrib & AM_DIR){
						char path_dir[40];
						strcpy(path_dir, path);
						p = strlen(path_dir);
						sprintf(&path_dir[p], "/%s", fno.fname);
						res = List_Files(path_dir, item);
						if (res != FR_OK) break;
						path[p] = 0;
					}else{
						item[i].item_list = fno;
						strcpy(item[i].path, path);
						strcat(item[i].path, "/");
						strcat(item[i].path, (item[i].item_list).fname);
						i++;

						}
					}

        }
        f_closedir(&dir);
    }
    else
    {
    	error = DIR_OPEN_ERR;
    	Error_Handler();
    }

    return res;
}

FRESULT Scan_Files (
    char* path,        /* Start node to be scanned (***also used as work area***) [IN] */
	uint8_t* count_files // przechowuje calkowita ilosc plikow na karcie SD [OUT]
)
{
    FRESULT res;
    DIR dir;
    static uint8_t i;
    uint8_t p = 0;
    static FILINFO fno;


    res = f_opendir(&dir, path);                       /* Open the directory */
    if (res == FR_OK) {
        for (;;) {
            res = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break; /* Break on error or end of dir */
				if (!strncmp(fno.fname, "System",6)){
					continue;
				}else{

					if (fno.fattrib & AM_DIR){
						char path_dir[40];
						strcpy(path_dir, path);
						p = strlen(path_dir);
						sprintf(&path_dir[p], "/%s", fno.fname);
						res = Scan_Files(path_dir, count_files);
						if (res != FR_OK) break;
						path[p] = 0;
					}else{
						i++;
						*count_files = i;
						}
					}

        }
        f_closedir(&dir);
    }else
    {
    	error = DIR_OPEN_ERR;
    	Error_Handler();
    }
    return res;
}

void Process_File( 			// modyfkuje zmienna ecg_data ktora jest globalna
	uint8_t file_nr, 		// numer pliku do przetworzenia [IN]
	FILNFO_ITEMS *items) 	// struktura opisujaca plik [IN]
{

	if(strstr(items[file_nr].item_list.fname, ".csv") != NULL){

	    if(Read_CSV(items[file_nr].path, &nr_col, &nr_row) != 0xFF) //uzyskaj ilosc wierszy i kolumn z pliku csv
	    	//uzylem 0xFF bo FRESULT nie serwuje takiego bledu ktory chcialbym obsluzyc
	    {

					// ---------------------- ZWALNIANIE pamieci po ecg_data --------------------//
					if(ecg_data_malloc == true)
					{
						memset(ecg_data, 0, sizeof(*ecg_data)*(nr_col)*(nr_row)); // wyczysc ecg_data samymi zerami

						for(int i=0; i<nr_row; i++){
						 free(ecg_data[i]); //uwolnienie pamieci
						}
						 free(ecg_data); //uwolnienie pamieci
						 ecg_data = NULL;
						 ecg_data_malloc = false;
					}
					// ------------------ ALOKACJA pamieci dla tablicy ecg_data -------------------//
					if(ecg_data_malloc == false)
					{
						ecg_data=(int16_t **)malloc(nr_row*sizeof(int16_t *));
						if (ecg_data == NULL) {
							error = BAD_ALLOC;
							Error_Handler();
						}
							 for(int i=0; i<nr_row; i++)
							 {
								 ecg_data[i]=(int16_t *)malloc(nr_col*sizeof(int16_t));
									if (ecg_data[i] == NULL) {
										error = BAD_ALLOC;
										 Error_Handler();
									}
							 }
							 ecg_data_malloc = true;
					}
					// -----------------------------------------------------------------------------//

					Parse_CSV(ecg_data, nr_row, items[file_nr].path); // wyciagnij dane z pliku csv i wpisz do ecg_data

					ECG_Scale();
			#ifdef UART
					for ( int i = 0; i < nr_row ; ++i, printf("\n") ){ // wypisz dane na log TEST
					 for ( int j = 0; j < nr_col; ++j){

						 UART_Print(ecg_data,NULL ,i ,j);
					 }
						 UART_Print(ecg_data,"\r\n" ,0 ,0);
					}

					UART_Print(NULL,"\r\n\n" ,0 ,0);
			#endif
	    }else
	    	UART_Print(NULL, "INVALID FILE\r\n", 0, 0);
	}else
		UART_Print(NULL, "INVALID FILE\r\n", 0, 0);

}
bool UART_Create_File(const char* filename){
	FIL file;
	char dir_name[10] = "PC Files"; //dedykowany folder na sztywno
	char path_to_file[30] = "PC Files"; //dedykowany folder na sztywno

	/*Sprawdz czy jest folder w glownym katalogu*/
	/*sprecyzowany przez sciezke, jesli nie to go stworz*/

	strcat(path_to_file, filename);

	if(f_stat(dir_name, NULL) != FR_OK){
			 f_mkdir(dir_name);
		 }else
		 {

			 if(f_stat(path_to_file, NULL) != FR_OK){

				 if(f_open(&file, path_to_file, FA_OPEN_ALWAYS | FA_READ | FA_WRITE) != FR_OK){
				 		f_close(&file);
					 	return false;
				 	}else
				 		f_close(&file);
				 		return true;

			 }
			 	//istnieje plik falsz
				 return false;
		 }

//jezeli musial stworzyc folder
	if(f_open(&file, path_to_file, FA_OPEN_ALWAYS | FA_READ | FA_WRITE) != FR_OK){
		f_close(&file);
		return false;
	}else
		f_close(&file);
		return true;
}

bool UART_Write_To_File(const char* data, const char* filename, UINT* bw){
FIL file;
char path[30] = "PC Files"; //dedykowany folder na sztywno

	strcat(path, filename);

	if(f_open(&file, path, FA_OPEN_EXISTING | FA_WRITE) != FR_OK){
		f_close(&file);
		return false;
	}else if(f_write(&file, data, strlen(data) , bw) != FR_OK){
		f_close(&file);
		return false;
	}else
		f_close(&file);
		return true;

}

void Simulate_IRQ(void){

	UINT bw = 0;

	char path [30] = "HUEHUE/notojazda.csv";
	char data[] = "Nie wiem jakis tekst byle co #yolo swag COMARCH";

	if(UART_Create_File(path) == false){
		UART_Print(NULL, "cossie zepsulo\n\r",0,0);
		//return;
	}
	if(UART_Write_To_File(data, path, &bw) == false){
		UART_Print(NULL, "cossie zepsulo\n\r",0,0);
		return;
	}



}

void UART_Print(
		int16_t** data,   //dane do wyswietlenia (ecg_data) [IN]
		const char* text, //text - znaki specjalne, jesli chcesz tylko dac nowe linie albo \r \t etc, jesli nie wstaw NULL [IN]
		uint16_t nr_row,  //index wiersza [IN]
		uint16_t nr_col)  //index kolumny [IN]
{

	char buff[32] ={0};
	uint16_t size=0;

	if(data != NULL || text != NULL){
		if(text == NULL){
			size = sprintf(buff ,"%d ",data[nr_row][nr_col]);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,size,10);
		}else
		{
			size = sprintf(buff, "%s", text);
			HAL_UART_Transmit(&huart2, (uint8_t*)buff,size,10);
		}
	}else{
		size = sprintf(buff, "%s", text);
		HAL_UART_Transmit(&huart2, (uint8_t*) buff, size, 10);
	}
}
