#-------------------------------------------------
#
# Project created by QtCreator 2016-07-28T11:37:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Nucleo
TEMPLATE = app


SOURCES += main.cpp\
        nucleo.cpp

HEADERS  += nucleo.h

FORMS    += nucleo.ui
