typedef enum KeywordCode KeywordCode;

unsigned char ucFindTokensInString(char *);
enum Result eStringKeyword(char [], KeywordCode *);
void DecodeTokens(void);
void DecodeMsg(char *);
enum Result eToken_GetKeywordCode(unsigned char, KeywordCode*);
enum Result eToken_GetNumber(unsigned char, unsigned int*);