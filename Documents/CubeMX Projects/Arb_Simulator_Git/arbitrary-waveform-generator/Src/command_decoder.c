#include "command_decoder.h"
#include "string.h"

#define NULL 0
#define MAX_KEYWORD_STRING_LTH 10
#define MAX_KEYWORD_NR 3
#define MAX_TOKEN_NR 3
#define SPACE 32

enum CompResult {DIFFERENT, EQUAL};
enum Result {OK, ERROR};
typedef enum TokenType {KEYWORD, NUMBER, STRING} TokenType;
typedef enum KeywordCode {CAL, GOTO, CALC} KeywordCode;
unsigned char ucTokenNr;

typedef union TokenValue
{
    KeywordCode eKeyword;
    unsigned int uiNumber;
    char* pcString;
} TokenValue;

typedef struct Keyword
{
    KeywordCode eCode;
    char cString[MAX_KEYWORD_STRING_LTH + 1];
} Keyword;

struct Keyword asKeywordList[MAX_KEYWORD_NR] =
{
    {CAL, "callib"},
    {GOTO, "goto"},
		{CALC, "calc"}
};

typedef struct Token
{
    TokenType eType;
    TokenValue uValue;
} Token;

Token asToken[MAX_TOKEN_NR];

typedef enum CharState {DELIMITER, TOKEN} CharState;

enum Result eToken_GetKeywordCode(unsigned char ucIndex, KeywordCode* eKey)
{
	if((0 != ucTokenNr) && (KEYWORD == asToken[ucIndex].eType))
	{
		*eKey = asToken[ucIndex].uValue.eKeyword;
		return OK;
	}
	else
		return ERROR;
}

enum Result eToken_GetNumber(unsigned char ucIndex, unsigned int* uiValue)
{
	if((0 != ucTokenNr) && (NUMBER == asToken[ucIndex].eType))
	{
		*uiValue = asToken[ucIndex].uValue.uiNumber;
		return OK;
	}
	else
		return ERROR;
}

unsigned char ucFindTokensInString(char *pcString)
{
    char *cTokens = pcString;
    unsigned char ucCharacterCounter;
    unsigned char ucTokenAmount = 0;
    CharState eCharState = TOKEN;
    CharState ePreviousCharState = DELIMITER;

    for(ucCharacterCounter = 0; ; ucCharacterCounter++)
    {
        switch(eCharState)
        {
        case TOKEN:
            if((MAX_TOKEN_NR <= ucTokenNr) || (NULL == cTokens[ucCharacterCounter]))
            {
                return ucTokenAmount;
            }
            else if(SPACE == cTokens[ucCharacterCounter])
            {
                eCharState = DELIMITER;
                ePreviousCharState = TOKEN;
            }
            else if(DELIMITER == ePreviousCharState)
            {
                asToken[ucTokenAmount++].uValue.pcString = &cTokens[ucCharacterCounter];
                ePreviousCharState = TOKEN;
            }
            break;
        case DELIMITER:
            if(NULL == cTokens[ucCharacterCounter])
            {
                return ucTokenAmount;
            }
            else if(SPACE != cTokens[ucCharacterCounter])
            {
                eCharState = TOKEN;
                ePreviousCharState = DELIMITER;
                ucCharacterCounter--;
            }
            break;
        }
    }
}

enum Result eStringKeyword(char pcStr[], enum KeywordCode *peKeywordCode)
{
    unsigned char ucKeywordNr;

    for(ucKeywordNr = 0; MAX_KEYWORD_NR > ucKeywordNr; ucKeywordNr++)
    {
        if(EQUAL == eCompareString(pcStr, asKeywordList[ucKeywordNr].cString))
        {
            *peKeywordCode = asKeywordList[ucKeywordNr].eCode;
            return OK;
        }
    }
    return ERROR;
};

void DecodeTokens()
{
    unsigned char ucTokenCounter;
    unsigned int uiHexValue;
    KeywordCode eKeyValue;

    for(ucTokenCounter = 0; ucTokenCounter < ucTokenNr; ucTokenCounter++)
    {
        if(OK == eStringKeyword(asToken[ucTokenCounter].uValue.pcString, &eKeyValue))
        {
            asToken[ucTokenCounter].uValue.eKeyword = eKeyValue;
            asToken[ucTokenCounter].eType = KEYWORD;
        }
        else if(OK == eHexStringToUInt(asToken[ucTokenCounter].uValue.pcString, &uiHexValue))
        {
            asToken[ucTokenCounter].uValue.uiNumber = uiHexValue;
            asToken[ucTokenCounter].eType = NUMBER;
        }
        else
        {
            asToken[ucTokenCounter].eType = STRING;
        }
    }
}

void DecodeMsg(char *pcString)
{
    unsigned char ucTokenCounter;

    ucTokenNr = ucFindTokensInString(pcString);
    for(ucTokenCounter = 0; ucTokenNr > ucTokenCounter; ucTokenCounter++)
    {
        ReplaceCharactersInString(asToken[ucTokenCounter].uValue.pcString, SPACE, NULL);
    }
    DecodeTokens();
}