#include "string.h"

#define NULL 0

enum CompResult {DIFFERENT, EQUAL};
enum Result {OK, ERROR};

void CopyString(char pcSource[], char pcDestination[])
{
    unsigned char ucCharacterCounter;

    for(ucCharacterCounter = 0; NULL != pcSource[ucCharacterCounter]; ucCharacterCounter++)
    {
        pcDestination[ucCharacterCounter] = pcSource[ucCharacterCounter];
    }
    pcDestination[ucCharacterCounter] = NULL;
}

enum CompResult eCompareString(char pcStr1[], char pcStr2[])
{
    unsigned char ucCharacterCounter;

    for(ucCharacterCounter = 0; pcStr1[ucCharacterCounter] == pcStr2[ucCharacterCounter]; ucCharacterCounter++)
    {
        if(NULL == pcStr1[ucCharacterCounter])
            return EQUAL;
    }
    return DIFFERENT;
}

void AppendString(char pcSource[], char pcDestination[])
{
    unsigned char ucDestinationLength = 0;

    while(NULL != pcDestination[ucDestinationLength])
    {
        ucDestinationLength++;
    }
    CopyString(pcSource, &pcDestination[ucDestinationLength]);
}

void ReplaceCharactersInString(char pcStr[], char cOldChar, char cNewChar)
{
    unsigned char ucCurrentIndex = 0;

    while(NULL != pcStr[ucCurrentIndex])
    {
        if(cOldChar == pcStr[ucCurrentIndex])
        {
            pcStr[ucCurrentIndex] = cNewChar;
        }
        ucCurrentIndex++;
    }
}

void UIntToHexStr(unsigned int uiValue, char pcStr[])
{
    unsigned int uiBitMask = 0xF000;
    unsigned char ucShift = 12;
    unsigned char ucCharacterCounter;

    pcStr[0] = '0';
    pcStr[1] = 'x';
    for(ucCharacterCounter = 2; 6 > ucCharacterCounter; ucCharacterCounter++)
    {
        if(10 > ((uiValue & uiBitMask) >> ucShift))
            pcStr[ucCharacterCounter] = ((uiValue & uiBitMask) >> ucShift) + '0';
        else
            pcStr[ucCharacterCounter] = ((uiValue & uiBitMask) >> ucShift) + ('A' - 10);
        uiBitMask = uiBitMask >> 4;
        ucShift -= 4;
    }
    pcStr[ucCharacterCounter] = NULL;
}

void AppendUIntToString(unsigned int uiValue, char pcDestination[])
{
    unsigned char ucDestinationLength = 0;

    while(NULL != pcDestination[ucDestinationLength])
    {
        ucDestinationLength++;
    }
    UIntToHexStr(uiValue, &pcDestination[ucDestinationLength]);
}

enum Result eHexStringToUInt(char pcStr[], unsigned int *puiValue)
{
    unsigned char ucMaxPower = -1;
    unsigned char ucCharacterCounter;
    unsigned char ucTestedCharacter;
    unsigned char ucTestPassed;
    unsigned int ucFactor;
    unsigned char ucRepetitions;

    if(('0' != pcStr[0]) || ('x' != pcStr[1]) || (NULL == pcStr[2]))
    {
        return ERROR;
    }
    for(ucCharacterCounter = 2; NULL != pcStr[ucCharacterCounter]; ucCharacterCounter++)
    {
        ucTestPassed = 0;
        for(ucTestedCharacter = '0'; 'F' >= ucTestedCharacter; ucTestedCharacter++)
        {
            if(pcStr[ucCharacterCounter] == ucTestedCharacter)
            {
                ucTestPassed = 1;
                break;
            }
            if('9' == ucTestedCharacter)
            {
                ucTestedCharacter = 'A';
                ucTestedCharacter--;
            }
        }
        if(0 == ucTestPassed)
        {
            return ERROR;
        }
        if(3 < ++ucMaxPower)
        {
            return ERROR;
        }
    }
    *puiValue = 0;
    for(ucCharacterCounter = 2; NULL != pcStr[ucCharacterCounter]; ucCharacterCounter++)
    {
        ucFactor = 1;
        for(ucRepetitions = ucMaxPower; ucRepetitions > 0; ucRepetitions--)
        {
            ucFactor *= 16;
        }
        if('0' <= pcStr[ucCharacterCounter] && '9' >= pcStr[ucCharacterCounter])
        {
            *puiValue += (pcStr[ucCharacterCounter] - '0') * ucFactor;
        }
        if('A' <= pcStr[ucCharacterCounter] && 'F' >= pcStr[ucCharacterCounter])
        {
            *puiValue += (pcStr[ucCharacterCounter] - ('A' - 10)) * ucFactor;
        }
        ucMaxPower--;
    }
    return OK;

};