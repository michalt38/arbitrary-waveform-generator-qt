################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/command_decoder.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/dac.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/dma.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/gpio.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/main.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/stm32f4xx_hal_msp.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/stm32f4xx_it.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/string.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/tim.c \
C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/usart.c 

OBJS += \
./Application/User/command_decoder.o \
./Application/User/dac.o \
./Application/User/dma.o \
./Application/User/gpio.o \
./Application/User/main.o \
./Application/User/stm32f4xx_hal_msp.o \
./Application/User/stm32f4xx_it.o \
./Application/User/string.o \
./Application/User/tim.o \
./Application/User/usart.o 

C_DEPS += \
./Application/User/command_decoder.d \
./Application/User/dac.d \
./Application/User/dma.d \
./Application/User/gpio.d \
./Application/User/main.d \
./Application/User/stm32f4xx_hal_msp.d \
./Application/User/stm32f4xx_it.d \
./Application/User/string.d \
./Application/User/tim.d \
./Application/User/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/command_decoder.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/command_decoder.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/command_decoder.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/dac.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/dac.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/dac.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/dma.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/dma.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/dma.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/gpio.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/gpio.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/main.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/main.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/main.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f4xx_hal_msp.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/stm32f4xx_hal_msp.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/stm32f4xx_hal_msp.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f4xx_it.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/stm32f4xx_it.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/stm32f4xx_it.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/string.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/string.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/string.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/tim.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/tim.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/tim.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/usart.o: C:/Users/Comarch/Documents/CubeMX\ Projects/Arb_Simulator/Src/usart.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Include" -I"C:/Users/Comarch/Documents/CubeMX Projects/Arb_Simulator/Drivers/CMSIS/Device/ST/STM32F4xx/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"Application/User/usart.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


