/********************************************************************************
** Form generated from reading UI file 'fileexporter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILEEXPORTER_H
#define UI_FILEEXPORTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FileExporter
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QProgressBar *progressBar;

    void setupUi(QDialog *FileExporter)
    {
        if (FileExporter->objectName().isEmpty())
            FileExporter->setObjectName(QStringLiteral("FileExporter"));
        FileExporter->resize(463, 58);
        verticalLayout = new QVBoxLayout(FileExporter);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(FileExporter);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        progressBar = new QProgressBar(FileExporter);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(24);

        verticalLayout->addWidget(progressBar);


        retranslateUi(FileExporter);

        QMetaObject::connectSlotsByName(FileExporter);
    } // setupUi

    void retranslateUi(QDialog *FileExporter)
    {
        FileExporter->setWindowTitle(QApplication::translate("FileExporter", "Dialog", 0));
        label->setText(QApplication::translate("FileExporter", "Exporting File...", 0));
    } // retranslateUi

};

namespace Ui {
    class FileExporter: public Ui_FileExporter {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILEEXPORTER_H
