/****************************************************************************
** Meta object code from reading C++ file 'arbgenerator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../arbitrary-waveform-generator-qt/arbgenerator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'arbgenerator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ArbGenerator_t {
    QByteArrayData data[18];
    char stringdata0[184];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ArbGenerator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ArbGenerator_t qt_meta_stringdata_ArbGenerator = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ArbGenerator"
QT_MOC_LITERAL(1, 13, 7), // "command"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 8), // "response"
QT_MOC_LITERAL(4, 31, 4), // "isOK"
QT_MOC_LITERAL(5, 36, 10), // "readSerial"
QT_MOC_LITERAL(6, 47, 12), // "setFrequency"
QT_MOC_LITERAL(7, 60, 4), // "freq"
QT_MOC_LITERAL(8, 65, 12), // "setAmplitude"
QT_MOC_LITERAL(9, 78, 4), // "ampl"
QT_MOC_LITERAL(10, 83, 17), // "setIncrementation"
QT_MOC_LITERAL(11, 101, 14), // "incrementation"
QT_MOC_LITERAL(12, 116, 15), // "setSamplingRate"
QT_MOC_LITERAL(13, 132, 4), // "rate"
QT_MOC_LITERAL(14, 137, 9), // "setSignal"
QT_MOC_LITERAL(15, 147, 13), // "setSerialPort"
QT_MOC_LITERAL(16, 161, 10), // "exportFile"
QT_MOC_LITERAL(17, 172, 11) // "sendCommand"

    },
    "ArbGenerator\0command\0\0response\0isOK\0"
    "readSerial\0setFrequency\0freq\0setAmplitude\0"
    "ampl\0setIncrementation\0incrementation\0"
    "setSamplingRate\0rate\0setSignal\0"
    "setSerialPort\0exportFile\0sendCommand"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ArbGenerator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       3,    1,   72,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   75,    2, 0x0a /* Public */,
       6,    1,   76,    2, 0x0a /* Public */,
       8,    1,   79,    2, 0x0a /* Public */,
      10,    1,   82,    2, 0x0a /* Public */,
      12,    1,   85,    2, 0x0a /* Public */,
      14,    1,   88,    2, 0x0a /* Public */,
      15,    0,   91,    2, 0x0a /* Public */,
      16,    0,   92,    2, 0x0a /* Public */,
      17,    1,   93,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    1,
    QMetaType::Void, QMetaType::Bool,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Double,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::QModelIndex,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    1,

       0        // eod
};

void ArbGenerator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ArbGenerator *_t = static_cast<ArbGenerator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->command((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->response((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->readSerial(); break;
        case 3: _t->setFrequency((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setAmplitude((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->setIncrementation((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->setSamplingRate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setSignal((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 8: _t->setSerialPort(); break;
        case 9: _t->exportFile(); break;
        case 10: _t->sendCommand((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ArbGenerator::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ArbGenerator::command)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ArbGenerator::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ArbGenerator::response)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject ArbGenerator::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ArbGenerator.data,
      qt_meta_data_ArbGenerator,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ArbGenerator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ArbGenerator::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ArbGenerator.stringdata0))
        return static_cast<void*>(const_cast< ArbGenerator*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ArbGenerator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void ArbGenerator::command(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ArbGenerator::response(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
