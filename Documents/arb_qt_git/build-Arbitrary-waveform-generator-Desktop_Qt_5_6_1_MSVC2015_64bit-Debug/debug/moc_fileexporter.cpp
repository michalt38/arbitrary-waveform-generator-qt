/****************************************************************************
** Meta object code from reading C++ file 'fileexporter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../arbitrary-waveform-generator-qt/fileexporter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fileexporter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FileExporterWorker_t {
    QByteArrayData data[11];
    char stringdata0[85];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FileExporterWorker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FileExporterWorker_t qt_meta_stringdata_FileExporterWorker = {
    {
QT_MOC_LITERAL(0, 0, 18), // "FileExporterWorker"
QT_MOC_LITERAL(1, 19, 8), // "finished"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 5), // "error"
QT_MOC_LITERAL(4, 35, 7), // "message"
QT_MOC_LITERAL(5, 43, 8), // "progress"
QT_MOC_LITERAL(6, 52, 4), // "done"
QT_MOC_LITERAL(7, 57, 5), // "total"
QT_MOC_LITERAL(8, 63, 7), // "command"
QT_MOC_LITERAL(9, 71, 7), // "process"
QT_MOC_LITERAL(10, 79, 5) // "abort"

    },
    "FileExporterWorker\0finished\0\0error\0"
    "message\0progress\0done\0total\0command\0"
    "process\0abort"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FileExporterWorker[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    1,   45,    2, 0x06 /* Public */,
       5,    2,   48,    2, 0x06 /* Public */,
       8,    1,   53,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   56,    2, 0x0a /* Public */,
      10,    0,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    6,    7,
    QMetaType::Void, QMetaType::QString,    8,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void FileExporterWorker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FileExporterWorker *_t = static_cast<FileExporterWorker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finished(); break;
        case 1: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->progress((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->command((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->process(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FileExporterWorker::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FileExporterWorker::finished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (FileExporterWorker::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FileExporterWorker::error)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (FileExporterWorker::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FileExporterWorker::progress)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (FileExporterWorker::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FileExporterWorker::command)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject FileExporterWorker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileExporterWorker.data,
      qt_meta_data_FileExporterWorker,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FileExporterWorker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FileExporterWorker::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FileExporterWorker.stringdata0))
        return static_cast<void*>(const_cast< FileExporterWorker*>(this));
    return QObject::qt_metacast(_clname);
}

int FileExporterWorker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void FileExporterWorker::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void FileExporterWorker::error(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void FileExporterWorker::progress(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void FileExporterWorker::command(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
struct qt_meta_stringdata_FileExporter_t {
    QByteArrayData data[13];
    char stringdata0[111];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FileExporter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FileExporter_t qt_meta_stringdata_FileExporter = {
    {
QT_MOC_LITERAL(0, 0, 12), // "FileExporter"
QT_MOC_LITERAL(1, 13, 12), // "readResponse"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 4), // "isOK"
QT_MOC_LITERAL(4, 32, 10), // "closeEvent"
QT_MOC_LITERAL(5, 43, 12), // "QCloseEvent*"
QT_MOC_LITERAL(6, 56, 5), // "event"
QT_MOC_LITERAL(7, 62, 7), // "onError"
QT_MOC_LITERAL(8, 70, 7), // "message"
QT_MOC_LITERAL(9, 78, 10), // "onFinished"
QT_MOC_LITERAL(10, 89, 10), // "onProgress"
QT_MOC_LITERAL(11, 100, 4), // "done"
QT_MOC_LITERAL(12, 105, 5) // "total"

    },
    "FileExporter\0readResponse\0\0isOK\0"
    "closeEvent\0QCloseEvent*\0event\0onError\0"
    "message\0onFinished\0onProgress\0done\0"
    "total"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FileExporter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x0a /* Public */,
       4,    1,   42,    2, 0x0a /* Public */,
       7,    1,   45,    2, 0x08 /* Private */,
       9,    0,   48,    2, 0x08 /* Private */,
      10,    2,   49,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   11,   12,

       0        // eod
};

void FileExporter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FileExporter *_t = static_cast<FileExporter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->readResponse((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->closeEvent((*reinterpret_cast< QCloseEvent*(*)>(_a[1]))); break;
        case 2: _t->onError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->onFinished(); break;
        case 4: _t->onProgress((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject FileExporter::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_FileExporter.data,
      qt_meta_data_FileExporter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FileExporter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FileExporter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FileExporter.stringdata0))
        return static_cast<void*>(const_cast< FileExporter*>(this));
    return QDialog::qt_metacast(_clname);
}

int FileExporter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
