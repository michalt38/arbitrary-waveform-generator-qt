/********************************************************************************
** Form generated from reading UI file 'serialdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERIALDIALOG_H
#define UI_SERIALDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SerialDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *com_tableView;
    QHBoxLayout *horizontalLayout;
    QLabel *baudRate_label;
    QSpinBox *baudRate_spinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SerialDialog)
    {
        if (SerialDialog->objectName().isEmpty())
            SerialDialog->setObjectName(QStringLiteral("SerialDialog"));
        SerialDialog->resize(344, 300);
        verticalLayout = new QVBoxLayout(SerialDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        com_tableView = new QTableView(SerialDialog);
        com_tableView->setObjectName(QStringLiteral("com_tableView"));

        verticalLayout->addWidget(com_tableView);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        baudRate_label = new QLabel(SerialDialog);
        baudRate_label->setObjectName(QStringLiteral("baudRate_label"));

        horizontalLayout->addWidget(baudRate_label);

        baudRate_spinBox = new QSpinBox(SerialDialog);
        baudRate_spinBox->setObjectName(QStringLiteral("baudRate_spinBox"));
        baudRate_spinBox->setMinimum(0);
        baudRate_spinBox->setMaximum(921600);
        baudRate_spinBox->setValue(9600);

        horizontalLayout->addWidget(baudRate_spinBox);


        verticalLayout->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(SerialDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SerialDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SerialDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SerialDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SerialDialog);
    } // setupUi

    void retranslateUi(QDialog *SerialDialog)
    {
        SerialDialog->setWindowTitle(QApplication::translate("SerialDialog", "Dialog", 0));
        baudRate_label->setText(QApplication::translate("SerialDialog", "Baud Rate", 0));
    } // retranslateUi

};

namespace Ui {
    class SerialDialog: public Ui_SerialDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERIALDIALOG_H
