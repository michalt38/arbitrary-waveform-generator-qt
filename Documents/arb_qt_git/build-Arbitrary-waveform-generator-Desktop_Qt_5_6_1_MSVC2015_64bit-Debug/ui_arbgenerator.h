/********************************************************************************
** Form generated from reading UI file 'arbgenerator.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARBGENERATOR_H
#define UI_ARBGENERATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ArbGenerator
{
public:
    QAction *actionSerial_port;
    QAction *actionExport_file;
    QAction *actionExit;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *settingsGroupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *freqLabel;
    QSpinBox *freqSpinBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *amplLabel;
    QDoubleSpinBox *amplSpinBox;
    QHBoxLayout *horizontalLayout_4;
    QLabel *incrLabel;
    QSpinBox *incrSpinBox;
    QHBoxLayout *horizontalLayout_6;
    QLabel *samplingRateLabel;
    QSpinBox *samplingRateSpinBox;
    QSpacerItem *verticalSpacer;
    QGroupBox *signalGroupBox;
    QHBoxLayout *horizontalLayout;
    QListView *signalList;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ArbGenerator)
    {
        if (ArbGenerator->objectName().isEmpty())
            ArbGenerator->setObjectName(QStringLiteral("ArbGenerator"));
        ArbGenerator->resize(470, 204);
        actionSerial_port = new QAction(ArbGenerator);
        actionSerial_port->setObjectName(QStringLiteral("actionSerial_port"));
        actionExport_file = new QAction(ArbGenerator);
        actionExport_file->setObjectName(QStringLiteral("actionExport_file"));
        actionExit = new QAction(ArbGenerator);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(ArbGenerator);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_5 = new QHBoxLayout(centralWidget);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        settingsGroupBox = new QGroupBox(centralWidget);
        settingsGroupBox->setObjectName(QStringLiteral("settingsGroupBox"));
        verticalLayout = new QVBoxLayout(settingsGroupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        freqLabel = new QLabel(settingsGroupBox);
        freqLabel->setObjectName(QStringLiteral("freqLabel"));

        horizontalLayout_2->addWidget(freqLabel);

        freqSpinBox = new QSpinBox(settingsGroupBox);
        freqSpinBox->setObjectName(QStringLiteral("freqSpinBox"));
        freqSpinBox->setMaximum(999999);
        freqSpinBox->setValue(55);

        horizontalLayout_2->addWidget(freqSpinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        amplLabel = new QLabel(settingsGroupBox);
        amplLabel->setObjectName(QStringLiteral("amplLabel"));

        horizontalLayout_3->addWidget(amplLabel);

        amplSpinBox = new QDoubleSpinBox(settingsGroupBox);
        amplSpinBox->setObjectName(QStringLiteral("amplSpinBox"));
        amplSpinBox->setDecimals(1);
        amplSpinBox->setMinimum(0.1);
        amplSpinBox->setMaximum(3.3);
        amplSpinBox->setSingleStep(0.1);
        amplSpinBox->setValue(3.3);

        horizontalLayout_3->addWidget(amplSpinBox);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        incrLabel = new QLabel(settingsGroupBox);
        incrLabel->setObjectName(QStringLiteral("incrLabel"));

        horizontalLayout_4->addWidget(incrLabel);

        incrSpinBox = new QSpinBox(settingsGroupBox);
        incrSpinBox->setObjectName(QStringLiteral("incrSpinBox"));
        incrSpinBox->setMaximum(999999);
        incrSpinBox->setValue(1);

        horizontalLayout_4->addWidget(incrSpinBox);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        samplingRateLabel = new QLabel(settingsGroupBox);
        samplingRateLabel->setObjectName(QStringLiteral("samplingRateLabel"));

        horizontalLayout_6->addWidget(samplingRateLabel);

        samplingRateSpinBox = new QSpinBox(settingsGroupBox);
        samplingRateSpinBox->setObjectName(QStringLiteral("samplingRateSpinBox"));
        samplingRateSpinBox->setMinimum(1);
        samplingRateSpinBox->setMaximum(999);
        samplingRateSpinBox->setSingleStep(1);
        samplingRateSpinBox->setValue(227);

        horizontalLayout_6->addWidget(samplingRateSpinBox);


        verticalLayout->addLayout(horizontalLayout_6);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_5->addWidget(settingsGroupBox);

        signalGroupBox = new QGroupBox(centralWidget);
        signalGroupBox->setObjectName(QStringLiteral("signalGroupBox"));
        horizontalLayout = new QHBoxLayout(signalGroupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        signalList = new QListView(signalGroupBox);
        signalList->setObjectName(QStringLiteral("signalList"));
        signalList->setEnabled(true);
        signalList->setEditTriggers(QAbstractItemView::EditKeyPressed);

        horizontalLayout->addWidget(signalList);


        horizontalLayout_5->addWidget(signalGroupBox);

        ArbGenerator->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ArbGenerator);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 470, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        ArbGenerator->setMenuBar(menuBar);
        statusBar = new QStatusBar(ArbGenerator);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ArbGenerator->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionSerial_port);
        menuFile->addAction(actionExport_file);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);

        retranslateUi(ArbGenerator);

        QMetaObject::connectSlotsByName(ArbGenerator);
    } // setupUi

    void retranslateUi(QMainWindow *ArbGenerator)
    {
        ArbGenerator->setWindowTitle(QApplication::translate("ArbGenerator", "ArbGenerator", 0));
        actionSerial_port->setText(QApplication::translate("ArbGenerator", "Serial port...", 0));
        actionExport_file->setText(QApplication::translate("ArbGenerator", "Export file...", 0));
        actionExit->setText(QApplication::translate("ArbGenerator", "Exit", 0));
        settingsGroupBox->setTitle(QApplication::translate("ArbGenerator", "Settings", 0));
        freqLabel->setText(QApplication::translate("ArbGenerator", "Frequency [Hz]", 0));
        amplLabel->setText(QApplication::translate("ArbGenerator", "Amplitude [V]", 0));
        incrLabel->setText(QApplication::translate("ArbGenerator", "Incrementation", 0));
        samplingRateLabel->setText(QApplication::translate("ArbGenerator", "Sampling rate[kHz]", 0));
        signalGroupBox->setTitle(QApplication::translate("ArbGenerator", "Signals", 0));
        menuFile->setTitle(QApplication::translate("ArbGenerator", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class ArbGenerator: public Ui_ArbGenerator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARBGENERATOR_H
