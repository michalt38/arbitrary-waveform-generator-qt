#include "arbgenerator.h"
#include "ui_arbgenerator.h"
#include <QMessageBox>
#include <QStringList>
#include <QUrl>
#include <QFileDialog>
#include "fileuploader.h"
#include "about.h"
#include <QDebug>
#include <QTime>

ArbGenerator::ArbGenerator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArbGenerator)
{
    ui->setupUi(this);
    this->setWindowTitle("Arbitrary waveform generator");

    connect(ui->actionSerial_port, &QAction::triggered, this, &ArbGenerator::setSerialPort);
    connect(ui->freqSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setFrequency(int)));
    connect(ui->amplSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setAmplitude(double)));
    connect(ui->incrSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setIncrementation(int)));
    connect(ui->actionExit, &QAction::triggered, qApp, &QApplication::quit);
    connect(ui->actionUpload_file, &QAction::triggered, this, &ArbGenerator::uploadFile);
    connect(this, &ArbGenerator::command, this, &ArbGenerator::sendCommand);
    connect(ui->samplingRateSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setSamplingRate(double)));
    connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(about()));
    connect(ui->actionSerial_reader, SIGNAL(triggered(bool)), this, SLOT(openSerialReader()));
    connect(ui->actionSignal_editor, SIGNAL(triggered(bool)), this, SLOT(openSignalEditor()));

    signalList << "Sine" << "Square" << "Triangle" << "Sawtooth" << "Sinc" << "Gaussian" << "Exponential" << "DC";
    model = new QStringListModel(this);
    model->setStringList(signalList);
    ui->signalList->setModel(model);

    m_lastAction = NONE;
    get_status = false;

    command_to_send = "";

    setSignalSettingsToDefault();
    ui->settingsToolBox->setCurrentIndex(0);

    connect(ui->signalList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(setSignal(QModelIndex)));

    serial = new QSerialPort(this);
    serialdialog = new SerialDialog(*serial, 0);
    responseBuffer = "";
    serialreader = new SerialReader(*this);
    uploader = new FileUploader(this);
    signaleditor = new SignalEditor(this, this);

    if(serialdialog->exec() == QDialog::Accepted)
    {
        serial->open(QSerialPort::ReadWrite);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
        QObject::connect(serial, SIGNAL(readyRead()), this, SLOT(readSerial()));
    }
    else
    {
        ArbGenerator::freezeUi();
    }
}

void ArbGenerator::openSerialReader()
{
    serialreader->startReading();
    serialreader->show();
}

void ArbGenerator::setSerialPort()
{
    serialdialog->refresh();
    serial->close();

    if(serialdialog->exec() == QDialog::Accepted)
    {
        serial->open(QSerialPort::ReadWrite);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
        connect(serial, SIGNAL(readyRead()), this, SLOT(readSerial()));
        ArbGenerator::releaseUi();
    }
}

void ArbGenerator::openSignalEditor()
{
    signaleditor->exec();
}

void ArbGenerator::readSerial()
{
    responseBuffer += QString::fromStdString(serial->readAll().toStdString());

    if(responseBuffer.contains("\r\n"))
    {
        while(1){
        QString newResponse = "";

        newResponse = responseBuffer.mid(responseBuffer.indexOf("\n")+1);
        responseBuffer = responseBuffer.mid(0, responseBuffer.indexOf("\n")+1);
        emit responseText(responseBuffer);

        qDebug() << responseBuffer;

        if(responseBuffer.contains("OK"))
        {
            emit response(true);
            if(get_status)
            {
                emit command("SHOW_STATUS\r");
                get_status = false;
            }
            command_to_send = "";
        }
        else if(responseBuffer.contains("ERR"))
        {
            emit response(false);
            emit command(command_to_send);
        }
        if(m_lastAction != NONE && get_status == false)
        {
            if(m_lastAction == FREQ)
            {
                if(responseBuffer.contains("INCREMENTATION"))
                {
                    responseBuffer.remove("INCREMENTATION: ");
                    responseBuffer.remove("\r\n");
                    int incr = responseBuffer.toInt();
                    ui->incrSpinBox->disconnect();
                    ui->incrSpinBox->setValue(incr);
                    connect(ui->incrSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setIncrementation(int)));
                }
                if(responseBuffer.contains("PRESCALER"))
                {
                    responseBuffer.remove("PRESCALER: ");
                    responseBuffer.remove("\r\n");
                    double psc = responseBuffer.toInt();
                    ui->samplingRateSpinBox->disconnect();
                    double rate = 180/(2*(psc+1));
                    ui->samplingRateSpinBox->setValue(rate);
                    connect(ui->samplingRateSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setSamplingRate(double)));
                    m_lastAction = NONE;
                }
            }
            if(m_lastAction == PSC || m_lastAction == INCR)
            {
                if(responseBuffer.contains("FREQUENCY"))
                {
                    responseBuffer.remove("FREQUENCY: ");
                    responseBuffer.remove("Hz\r\n");
                    int freq = responseBuffer.toInt();
                    ui->freqSpinBox->disconnect();
                    ui->freqSpinBox->setValue(freq);
                    connect(ui->freqSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setFrequency(int)));
                    m_lastAction = NONE;
                }
            }
        }
        responseBuffer = "";
        command_to_send = "";

        responseBuffer += newResponse;
        if(!responseBuffer.contains("\r\n")) break;
        }
    }
}

void ArbGenerator::about()
{
    About *about = new About;
    about->exec();
    delete about;
}

void ArbGenerator::setSignalSettingsToDefault()
{
    ui->cycleLabel->hide();

    ui->cycleSpinBox->hide();
    ui->cycleSpinBox->disconnect();
    ui->cycleSpinBox->setValue(50);
    connect(ui->cycleSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setDutyCycle(int)));

    ui->signalTypeLabel->hide();

    ui->signalTypeComboBox->hide();
    ui->signalTypeComboBox->disconnect();
    ui->signalTypeComboBox->setCurrentIndex(0);
    connect(ui->signalTypeComboBox, SIGNAL(activated(QString)), this, SLOT(setSignalType(QString)));

    ui->righLabel->hide();
    ui->leftLabel->hide();

    ui->sawtoothSlider->hide();
    ui->sawtoothSlider->disconnect();
    ui->sawtoothSlider->setValue(100);
    connect(ui->sawtoothSlider, SIGNAL(valueChanged(int)), this, SLOT(setDutyCycle(int)));
}

void ArbGenerator::setSignalType(QString signal_type)
{
    emit command("SET_PARAM " + signal_type.toUpper() + "\r");
}

void ArbGenerator::setFactor(double factor)
{
    emit command("SET_PARAM FACTOR " + QString::number(factor) + "\r");
}

void ArbGenerator::setExpectedValue(double value)
{
    emit command("SET_PARAM EXP_VALUE " + QString::number(value) + "\r");
}

void ArbGenerator::setVariance(double var)
{
    emit command("SET_PARAM VARIANCE " + QString::number(var) + "\r");
}

void ArbGenerator::reverseSignal()
{
    emit command("SET_PARAM REVERSE\r");
}

void ArbGenerator::setDutyCycle(int cycle)
{
    emit command("SET_PARAM DUTY_CYCLE " + QString::number(cycle) + "\r");
}

void ArbGenerator::releaseUi()
{
    ui->settingsGroupBox->setEnabled(true);
    ui->signalGroupBox->setEnabled(true);
    ui->actionUpload_file->setEnabled(true);
    ui->actionSerial_reader->setEnabled(true);
}

void ArbGenerator::setSamplingRate(double rate)
{
    rate *= 1000;
    int psc =(int)(180000/(2*rate)) - 1;
    emit command("SET_PSC " + QString::number(psc) + "\r");
    m_lastAction = PSC;
    get_status = true;
}

void ArbGenerator::freezeUi()
{
    ui->settingsGroupBox->setDisabled(true);
    ui->signalGroupBox->setDisabled(true);
    ui->actionUpload_file->setDisabled(true);
    ui->actionSerial_reader->setDisabled(true);
}

void ArbGenerator::sendCommand(QString command)
{
    if(serial->isWritable())
    {
        serial->write(command.toStdString().c_str());
        command_to_send = command;
    }else
    {
        QMessageBox::critical(this, "Send Command Error", "Couldn't write to serial");
        command_to_send = "";
    }
}

void ArbGenerator::setSignal(QModelIndex modelIndex)
{
    emit command("SET_SIGNAL " + signalList.at(modelIndex.row()).toUpper() + "\r");
    setSignalSettingsToDefault();
    if(signalList.at(modelIndex.row()) == "Square")
    {
        ui->cycleLabel->show();
        ui->cycleSpinBox->show();
    }
    if(signalList.at(modelIndex.row()) == "Sawtooth")
    {
        ui->leftLabel->show();
        ui->righLabel->show();
        ui->sawtoothSlider->show();
    }
    if(signalList.at(modelIndex.row()) == "Sinc")
    {
        ui->cycleLabel->show();
        ui->cycleSpinBox->show();
    }
    if(signalList.at(modelIndex.row()) == "Gaussian")
    {
        ui->cycleLabel->show();
        ui->cycleSpinBox->show();
    }
    if(signalList.at(modelIndex.row()) == "Exponential")
    {
        ui->signalTypeComboBox->show();
        ui->signalTypeLabel->show();
    }
}

void ArbGenerator::setFrequency(int freq)
{
    emit command("SET_FREQ " + QString::number(freq) + "\r");
    m_lastAction = FREQ;
    get_status = true;
}

void ArbGenerator::setAmplitude(double ampl)
{
    emit command("SET_AMPL " + QString::number(ampl) + "\r");
}

void ArbGenerator::setIncrementation(int incrementation)
{
    emit command("SET_INCR " + QString::number(incrementation) + "\r");
    m_lastAction = INCR;
    get_status = true;
}

void ArbGenerator::uploadFile()
{
    QUrl url = QFileDialog::getOpenFileUrl(this, "Open file", QUrl(""), "Text file (*.txt) (*txt)");
    QString path = url.path().remove(0, 1);

    if(path.isEmpty()) return;

    //connect(this, &ArbGenerator::response, uploader, &FileUploader::readResponse);

    uploader->uploadFile(path);
    //uploader->disconnect();

    setSignalSettingsToDefault();
}

void ArbGenerator::uploadFinished()
{
    if(!signalList.contains("Arbitrary"))
    {
        signalList << "Arbitrary";
        model->setStringList(signalList);
        ui->signalList->setModel(model);
    }
}

void ArbGenerator::uploadAborted()
{
    emit command("UPLOAD_ABORT\r");

    if(signalList.contains("Arbitrary"))
    {
        signalList.pop_back();
        model->setStringList(signalList);
        ui->signalList->setModel(model);
    }
}

void ArbGenerator::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    qApp->closeAllWindows();
}

ArbGenerator::~ArbGenerator()
{
    serial->close();
    delete model;
    delete serialdialog;
    delete serialreader;
    delete uploader;
    delete signaleditor;
    delete serial;
    delete ui;
}
