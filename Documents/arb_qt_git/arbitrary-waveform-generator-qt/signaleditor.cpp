#include "signaleditor.h"
#include "ui_signaleditor.h"
#include "setvaluemessage.h"
#include "math.h"
#include <QtAlgorithms>
#include <QDebug>
#include <QMessageBox>

SignalEditor::SignalEditor(ArbGenerator *generator, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SignalEditor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);
    this->setWindowTitle("Signal editor");

    m_dataIndex = 0;
    m_scaledProbesNum = 1000;
    m_plotClicked = false;
    m_nodesSelected = false;

    saveButton = ui->panelWidget->findChild<QPushButton*>("saveButton");
    loadButton = ui->panelWidget->findChild<QPushButton*>("loadButton");
    closeButton = ui->panelWidget->findChild<QPushButton*>("closeButton");
    periodityButton = ui->panelWidget->findChild<QPushButton*>("periodityButton");
    resetButton = ui->panelWidget->findChild<QPushButton*>("resetButton");
    maximizeButton = ui->panelWidget->findChild<QPushButton*>("maximizeButton");
    uploadButton = ui->panelWidget->findChild<QPushButton*>("uploadButton");
    scaledProbesSpinBox = ui->panelWidget->findChild<QSpinBox*>("scaledProbesSpinBox");

    statusBar = new QStatusBar(this);
    statusBar->setMaximumHeight(20);
    ui->statusBarLayout->addWidget(statusBar);

    signaluploader = new SignalUploader(generator, this);

    for (int i=0; i <= 4095; ++i)
    {
      x.append(i);
      y.append(2047);
    }

    ui->customPlot->addGraph();

    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->xAxis->setLabel("Probe");
    ui->customPlot->yAxis->setLabel("Value");
    ui->customPlot->xAxis->setRange(0, 4095);
    ui->customPlot->yAxis->setRange(0, 4095);
    ui->customPlot->replot();

    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->customPlot->setInteractions(QCP::iSelectPlottables | QCP::iRangeZoom | QCP::iRangeDrag);

    connect(ui->customPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMoved(QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mouseClicked(QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseReleased(QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(mouseDoubleClick(QMouseEvent*)), this, SLOT(mouseDoubleClicked(QMouseEvent*)));

    connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveSignal()));
    connect(loadButton, SIGNAL(clicked(bool)), this, SLOT(loadSignal()));
    connect(resetButton, SIGNAL(clicked(bool)), this, SLOT(resetSignal()));
    connect(periodityButton, SIGNAL(clicked(bool)), this, SLOT(makeSignalPeriodic()));
    connect(scaledProbesSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setScaledProbesNum(int)));
    connect(closeButton, SIGNAL(clicked(bool)), this, SLOT(close()));
    connect(maximizeButton, SIGNAL(clicked(bool)), this, SLOT(maximizeSignal()));
    connect(uploadButton, SIGNAL(clicked(bool)), this, SLOT(uploadSignal()));
    connect(ui->customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(plotSelected()));
    connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(removePoints()));
}

SignalEditor::~SignalEditor()
{
    delete ui;
    delete statusBar;
    delete signaluploader;
}

void SignalEditor::uploadSignal()
{
    signaluploader->uploadSignal(y);
}

void SignalEditor::maximizeSignal()
{
    int min = y[0];
    int max = y[0];
    for(int i = 1; i < 4096; i++)
    {
        if(y[i] < min)
            min = y[i];

        if(y[i] > max)
            max = y[i];
    }

    for(int i = 0; i < 4096; i++)
    {
        y[i] -= min;
    }
    max -= min;;

    double scale = 4095/(double)max;
    for(int i = 0; i < 4096; i++)
    {
        y[i] = (int)((double)(y[i])*scale);
    }

    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->replot();
}

void SignalEditor::removePoints()
{
    if(x_nodes.size() != 0)
    {
        x_nodes.clear();
        y_nodes.clear();
        m_nodesSelected = false;
        ui->customPlot->removeGraph(1);
        ui->customPlot->replot();
    }
}

void SignalEditor::mouseDoubleClicked(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton) return;
    if(!ui->customPlot->graph(0)->selected()) return;

    qDebug() << "here";

    int x_coordinate = (int)ui->customPlot->xAxis->pixelToCoord(event->pos().x());

    if(x_nodes.size() == 0)
    {
        ui->customPlot->addGraph();
        QCPScatterStyle style;
        style.setSize(7);
        style.setShape(QCPScatterStyle::ssDisc);
        style.setPen(QPen(Qt::red));
        style.setBrush(Qt::red);
        ui->customPlot->graph(1)->setLineStyle(QCPGraph::lsNone);
        ui->customPlot->graph(1)->setScatterStyle(style);
        ui->customPlot->graph(1)->setSelectable(QCP::stNone);
    }

    if(x_nodes.size() <= 1)
    {
        x_nodes.append(x_coordinate);
        y_nodes.append(y[x_coordinate]);

        ui->customPlot->graph(1)->setData(x_nodes, y_nodes);
        ui->customPlot->replot();
    }

    if(x_nodes.size() == 2)
    {
        if(x_nodes.at(1) < x_nodes.at(0))
        {
            qSwap(x_nodes[0], x_nodes[1]);
            qSwap(y_nodes[0], y_nodes[1]);
        }
        m_nodesSelected = true;
    }
}

void SignalEditor::resetSignal()
{
    y.clear();
    x_nodes.clear();
    y_nodes.clear();
    m_nodesSelected = false;

    for(int i = 0; i < 4096; i++)
    {
        y.append(2047);
    }

    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->removeGraph(1);
    ui->customPlot->replot();
}

void SignalEditor::makeSignalPeriodic()
{
    SetValueMessage msg("Set Value", "Repeated value:", 0, 4095);
    if(msg.exec() == QDialog::Rejected) return;

    int repeatedValue = msg.getValue();

    int scaledProbes = m_scaledProbesNum;

    if(m_scaledProbesNum > 4094)
        m_scaledProbesNum = 4094;

    double factor = (1.0/(double)(m_scaledProbesNum/2));
    int it = (m_scaledProbesNum/2) - 1;

    for(int i = 1; i <= (m_scaledProbesNum/2); i++)
    {
        y[i] = y[i] + (int)((repeatedValue - y[0])*factor*it--);

        if(y[i] < 0)
            y[i] = 0;

        if(y[i] > 4095)
            y[i] = 4095;
    }

    y[0] = repeatedValue;

    it = (m_scaledProbesNum/2) - 1;

    for(int i = 4094; i >= 4094 - (m_scaledProbesNum/2); i--)
    {
        y[i] = y[i] + (int)((repeatedValue - y[4095])*factor*it--);

        if(y[i] < 0)
            y[i] = 0;

        if(y[i] > 4095)
            y[i] = 4095;
    }

    y[4095] = repeatedValue;

    m_scaledProbesNum = scaledProbes;

    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->replot();
}

void SignalEditor::plotSelected()
{
    if(ui->customPlot->graph(0)->selected())
        ui->customPlot->setInteraction(QCP::iRangeDrag, false);
    else
        ui->customPlot->setInteraction(QCP::iRangeDrag, true);
}

void SignalEditor::mouseClicked(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton) return;
    if(!(ui->customPlot->graph(0)->selected() && !m_plotClicked)) return;
    if(x_nodes.size() == 1) return;

    int x_coordinate = ui->customPlot->xAxis->pixelToCoord(event->pos().x());
    if(x_coordinate < 0 || x_coordinate > 4095) return;
    if(m_nodesSelected && (x_coordinate <= x_nodes.at(0) || x_coordinate >= x_nodes.at(1))) return;

    qDebug() << x_coordinate;
    int index = x.indexOf(x_coordinate);
    qDebug() << index;
    m_dataIndex = index;

    if(!m_nodesSelected)
    {
        for(int i = -(m_scaledProbesNum/2); i <= (m_scaledProbesNum/2); i++)
        {
            if(m_dataIndex + i < 0 || m_dataIndex + i > 4095)
                y_points.append(0);
            else
                y_points.append(y[m_dataIndex + i]);
        }
    }
    else
    {
        for(int i = x_nodes.at(0) + 1; i < x_nodes.at(1); i++)
        {
            if(i < 0 || i > 4095)
                y_points.append(0);
            else
                y_points.append(y[i]);
        }
    }

    m_plotClicked = true;
}

void SignalEditor::mouseMoved(QMouseEvent *event)
{
    int x_coordinate = (int)ui->customPlot->xAxis->pixelToCoord(event->pos().x());
    int y_coordinate = (int)ui->customPlot->yAxis->pixelToCoord(event->pos().y());
    statusBar->showMessage(QString("X: ") + QString::number(x_coordinate) + QString(" Y: ") + QString::number(y_coordinate));

    if(x_coordinate < 0 || x_coordinate > 4095) return;
    if(!m_plotClicked) return;

    y[m_dataIndex] = y_coordinate;

    if(y[m_dataIndex] < 0)
        y[m_dataIndex] = 0;

    if(y[m_dataIndex] > 4095)
        y[m_dataIndex] = 4095;

    if(m_scaledProbesNum == 0)
    {
        ui->customPlot->graph(0)->setData(x, y);
        ui->customPlot->replot();
        return;
    }

    double factor;
    int it;

    if(m_nodesSelected)
    {
        factor = (1.0/(double)(abs(m_dataIndex - x_nodes.at(0) - 1)));
        it = 0;

        for(int i = x_nodes.at(0) + 1; i < m_dataIndex; i++)
        {
            y[i] = y_points.at(i - x_nodes.at(0) - 1) + (int)((y[m_dataIndex] - y_points.at(abs(m_dataIndex - x_nodes.at(0) - 1)))*factor*it++);

            if(y[i] < 0)
                y[i] = 0;

            if(y[i] > 4095)
                y[i] = 4095;
        }

        factor = (1.0/(double)(abs(x_nodes.at(1) - m_dataIndex - 1)));
        it = abs(x_nodes.at(1) - m_dataIndex - 1);

        for(int i = m_dataIndex + 1; i < x_nodes.at(1); i++)
        {
            y[i] = y_points.at(i - m_dataIndex - 1 + (m_dataIndex - x_nodes.at(0))) + (int)((y[m_dataIndex] - y_points.at(abs(m_dataIndex - x_nodes.at(0) - 1)))*factor*it--);

            if(y[i] < 0)
                y[i] = 0;

            if(y[i] > 4095)
                y[i] = 4095;
        }

        if(x_coordinate != m_dataIndex && (x_coordinate < x_nodes.at(1) && x_coordinate > x_nodes.at(0)))
        {
            int len = abs(x_nodes.at(1) - x_coordinate);

            int shift = abs(x_coordinate - m_dataIndex);

            if(len != 1)
                factor = (1.0/(double)(len - 1));
            else
                factor = 1;
            it = len - 1;

            int newValuesRight[len];

            for(int i = 0; i < len; i++)
            {
                if(x_coordinate < m_dataIndex)
                {
                    if((int)(x_coordinate + shift*factor*it + i) > 4095)
                        newValuesRight[i] = y[4095];
                    else if((int)(x_coordinate + shift*factor*it + i) < 0)
                        newValuesRight[i] = y[0];
                    else
                        newValuesRight[i] = y[(int)(x_coordinate + shift*factor*it-- + i)];
                }
                else
                {
                    if((int)(x_coordinate - shift*factor*it + i) > 4095)
                        newValuesRight[i] = y[4095];
                    else if((int)(x_coordinate - shift*factor*it + i) < 0)
                        newValuesRight[i] = y[0];
                    else
                        newValuesRight[i] = y[(int)(x_coordinate - shift*factor*it-- + i)];
                }
            }

            int rightLen = len;

            len = (x_nodes.at(1) - x_nodes.at(0)) - len - 1;
            factor = (1.0/(double)(len - 1));
            it = len - 1;
            int newValuesLeft[len];

            for(int i = len - 1; i >= 0; i--)
            {
                if(x_coordinate < m_dataIndex)
                {
                    if((int)(x_coordinate - len + shift*factor*it + i) < 0)
                        newValuesLeft[i] = y[0];
                    else if((int)(x_coordinate - len + shift*factor*it + i) > 4095)
                        newValuesLeft[i] = y[4095];
                    else
                        newValuesLeft[i] = y[(int)(x_coordinate - len + shift*factor*it-- + i)];
                }
                else
                {
                    if((int)(x_coordinate - len - shift*factor*it + i) < 0)
                        newValuesLeft[i] = y[0];
                    else if((int)(x_coordinate - len - shift*factor*it + i) > 4095)
                        newValuesLeft[i] = y[4095];
                    else
                        newValuesLeft[i] = y[(int)(x_coordinate - len - shift*factor*it-- + i)];
                }
            }

            for(int i = 0; i < len; i++)
            {
                if(x_coordinate - len + i < 0)
                    continue;

                y[x_coordinate - len + i] = newValuesLeft[i];
            }

            for(int i = 0; i < rightLen; i++)
            {
                if(x_coordinate + i > 4095)
                    continue;

                y[x_coordinate + i] = newValuesRight[i];
            }
        }
        else if(x_coordinate != m_dataIndex && ((x_coordinate >= x_nodes.at(1)) || (x_coordinate <= x_nodes.at(0))))
        {
            int len = (x_nodes.at(1) - x_nodes.at(0)) - 1;
            int shift;

            if(x_coordinate < m_dataIndex)
            {
                shift = abs(m_dataIndex - x_nodes.at(0) - 1);
                factor = (1.0/(double)(len - 1));
                it = len - 1;

                int newValuesRight[len];

                for(int i = 0; i < len; i++)
                {
                    if((int)(x_nodes.at(0) + 1 + shift*factor*i + i) > 4095)
                        newValuesRight[i] = y[4095];
                    else if((int)(x_nodes.at(0) + 1 + shift*factor*it + i) < 0)
                        newValuesRight[i] = y[0];
                    else
                        newValuesRight[i] = y[(int)(x_nodes.at(0) + 1 + shift*factor*it-- + i)];
                }

                for(int i = 0; i < len; i++)
                {
                    if(x_nodes.at(0) + 1 + i > 4095)
                        continue;

                    y[x_nodes.at(0) + 1 + i] = newValuesRight[i];
                }
            }else
            {
                shift = abs(x_nodes.at(1) - m_dataIndex - 1);
                factor = (1.0/(double)(len - 1));
                it = len - 1;
                int newValuesLeft[len];

                for(int i = len - 1; i >= 0; i--)
                {
                    if((int)(x_nodes.at(1) - 1 - len - shift*factor*it + i) < 0)
                        newValuesLeft[i] = y[0];
                    else if((int)(x_nodes.at(1) - 1 - len - shift*factor*it + i) > 4095)
                        newValuesLeft[i] = y[4095];
                    else
                        newValuesLeft[i] = y[(int)(x_nodes.at(1) - 1 - len - shift*factor*it-- + i)];
                }

                for(int i = 0; i < len; i++)
                {
                    if(x_nodes.at(1) - 1 - len + i < 0)
                        continue;

                    y[x_nodes.at(1) - 1 - len + i] = newValuesLeft[i];
                }
            }
        }

        ui->customPlot->graph(0)->setData(x, y);
        ui->customPlot->replot();
        return;
    }

    factor = (1.0/(double)(m_scaledProbesNum/2));
    it = 0;

    for(int i = -(m_scaledProbesNum/2); i <= (m_scaledProbesNum/2); i++)
    {
        if(i == 0) continue;

        if(m_dataIndex + i < 0 || m_dataIndex + i > 4095)
        {
            i < 0 ? it++ : it--;
            continue;
        }

        y[m_dataIndex + i] = y_points.at(i + (m_scaledProbesNum/2)) + (int)((y[m_dataIndex] - y_points.at((m_scaledProbesNum/2)))*factor*(i < 0 ? it++ : --it));

        if(y[m_dataIndex + i] < 0)
            y[m_dataIndex + i] = 0;

        if(y[m_dataIndex + i] > 4095)
            y[m_dataIndex + i] = 4095;
    }

    if(x_coordinate != m_dataIndex && (x_coordinate < m_dataIndex + (m_scaledProbesNum/2) && (x_coordinate > m_dataIndex - (m_scaledProbesNum/2))))
    {
        int len;

        if(x_coordinate < m_dataIndex)
            len = abs(x_coordinate - m_dataIndex) + (m_scaledProbesNum/2);
        else
            len = abs(abs(x_coordinate - m_dataIndex) + (m_scaledProbesNum/2) - m_scaledProbesNum);

        int shift = abs(x_coordinate - m_dataIndex);

        factor = (1.0/(double)(len - 1));
        it = len - 1;

        int newValuesRight[len];

        for(int i = 0; i < len; i++)
        {
            if(x_coordinate < m_dataIndex)
            {
                if((int)(x_coordinate + shift*factor*it + i) > 4095)
                    newValuesRight[i] = y[4095];
                else if((int)(x_coordinate + shift*factor*it + i) < 0)
                    newValuesRight[i] = y[0];
                else
                    newValuesRight[i] = y[(int)(x_coordinate + shift*factor*it-- + i)];
            }
            else
            {
                if((int)(x_coordinate - shift*factor*it + i) > 4095)
                    newValuesRight[i] = y[4095];
                else if((int)(x_coordinate - shift*factor*it + i) < 0)
                    newValuesRight[i] = y[0];
                else
                    newValuesRight[i] = y[(int)(x_coordinate - shift*factor*it-- + i)];
            }
        }

        int rightLen = len;

        len = m_scaledProbesNum - len;
        factor = (1.0/(double)(len - 1));
        it = len - 1;
        int newValuesLeft[len];

        for(int i = len - 1; i >= 0; i--)
        {
            if(x_coordinate < m_dataIndex)
            {
                if((int)(x_coordinate - len + shift*factor*it + i + 1) < 0)
                    newValuesLeft[i] = y[0];
                else if((int)(x_coordinate - len + shift*factor*it + i + 1) > 4095)
                    newValuesLeft[i] = y[4095];
                else
                    newValuesLeft[i] = y[(int)(x_coordinate - len + shift*factor*it-- + i + 1)];
            }
            else
            {
                if((int)(x_coordinate - len - shift*factor*it + i + 1) < 0)
                    newValuesLeft[i] = y[0];
                else if((int)(x_coordinate - len - shift*factor*it + i + 1) > 4095)
                    newValuesLeft[i] = y[4095];
                else
                    newValuesLeft[i] = y[(int)(x_coordinate - len - shift*factor*it-- + i + 1)];
            }
        }

        for(int i = 0; i < len; i++)
        {
            if(x_coordinate - len + i + 1 < 0)
                continue;

            y[x_coordinate - len + i + 1] = newValuesLeft[i];
        }

        for(int i = 0; i < rightLen; i++)
        {
            if(x_coordinate + i > 4095)
                continue;

            y[x_coordinate + i] = newValuesRight[i];
        }
    }else if(x_coordinate != m_dataIndex && (x_coordinate >= m_dataIndex + (m_scaledProbesNum/2) || x_coordinate <= m_dataIndex - (m_scaledProbesNum/2)))
    {
        int len = m_scaledProbesNum;
        int shift =(m_scaledProbesNum/2);

        if(x_coordinate < m_dataIndex)
        {
            len = m_scaledProbesNum;
            factor = (1.0/(double)(len - 1));
            it = len - 1;

            int newValuesRight[len];

            for(int i = 0; i < len; i++)
            {
                if((int)((m_dataIndex - (m_scaledProbesNum/2)) + shift*factor*it + i) > 4095)
                    newValuesRight[i] = y[4095];
                else if((int)((m_dataIndex - (m_scaledProbesNum/2)) + shift*factor*it + i) < 0)
                    newValuesRight[i] = y[0];
                else
                    newValuesRight[i] = y[(int)((m_dataIndex - (m_scaledProbesNum/2)) + shift*factor*it-- + i)];
            }

            for(int i = 0; i < len; i++)
            {
                if((m_dataIndex - (m_scaledProbesNum/2)) + i > 4095)
                    continue;

                y[(m_dataIndex - (m_scaledProbesNum/2)) + i] = newValuesRight[i];
            }
        }else
        {
            factor = (1.0/(double)(len - 1));
            it = len - 1;
            int newValuesLeft[len];

            for(int i = len - 1; i >= 0; i--)
            {
                if((int)((m_dataIndex + (m_scaledProbesNum/2)) - len - shift*factor*it + i + 1) < 0)
                    newValuesLeft[i] = y[0];
                else if((int)((m_dataIndex + (m_scaledProbesNum/2)) - len - shift*factor*it + i + 1) > 4095)
                    newValuesLeft[i] = y[4095];
                else
                    newValuesLeft[i] = y[(int)((m_dataIndex + (m_scaledProbesNum/2)) - len - shift*factor*it-- + i + 1)];
            }

            for(int i = 0; i < len; i++)
            {
                if((m_dataIndex + (m_scaledProbesNum/2)) - len + i + 1 < 0)
                    continue;

                y[(m_dataIndex + (m_scaledProbesNum/2)) - len + i + 1] = newValuesLeft[i];
            }
        }
    }
    qDebug() << y[m_dataIndex];
    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->replot();
}

void SignalEditor::mouseReleased(QMouseEvent *event)
{
    Q_UNUSED(event);
    m_plotClicked = false;
    y_points.clear();
}

void SignalEditor::saveSignal()
{
    QString path = QFileDialog::getSaveFileName(this, tr("Save file"), "", "Text file (*.txt)");
    if(path.isEmpty()) return;

    QFile file(path);
    QTextStream outstream(&file);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, "Error", tr("Unable to open file\n%1").arg(file.errorString()));
        return;
    }

    for(int i = 0; i <= 4095; i++)
    {
        outstream << y[i] << "\n";
    }

    file.close();

    qDebug() << "saved";
    statusBar->showMessage("File saved");
}

void SignalEditor::loadSignal()
{
    QUrl url = QFileDialog::getOpenFileUrl(this, "Open file", QUrl(""), "Text file (*.txt) (*txt)");
    QString path = url.path().remove(0, 1);
    if(path.isEmpty()) return;

    QFile file(path);
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Error", tr("Unable to open file\n%1").arg(file.errorString()));
        return;
    }

    QTextStream  instream(&file);
    QString new_value = "";
    int index = 0;
    bool isOK;
    int values[4096];

    while(1)
    {
        if(instream.atEnd()) break;

        if(index > 4095)
        {
            QMessageBox::critical(this, "Error", tr("Too much probes"));
            file.close();
            return;
        }

        new_value = instream.readLine();
        values[index++] = new_value.toInt(&isOK);

        if(!isOK)
        {
            QMessageBox::critical(this, "Error", tr("Incorrect value at %d line").arg(index-1));
            file.close();
            return;
        }

        if(values[index-1] < 0 || values[index-1] > 4095)
        {
            QMessageBox::critical(this, "Error", tr("Value is out of range at %d line").arg(index-1));
            file.close();
            return;
        }
    }

    if(index != 4096)
    {
        QMessageBox::critical(this, "Error", tr("Not enough probes"));
        file.close();
        return;
    }

    for(int i = 0; i <= 4095; i++)
    {
        y[i] = values[i];
    }

    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->replot();

    file.close();

    statusBar->showMessage("File loaded");
}

void SignalEditor::setScaledProbesNum(int probes)
{
    if(probes % 2 != 0)
    {
        probes += 1;
        ui->scaledProbesSpinBox->setValue(probes);
    }
    else
        m_scaledProbesNum = probes;
}
