#include "fileuploader.h"
#include "ui_FileUploader.h"

#include <QFileInfo>
#include <QThread>
#include <QDebug>
#include <QMutex>
#include <QMessageBox>
#include "arbgenerator.h"

FileUploader::FileUploader(ArbGenerator *generator, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileUploader),
    worker(nullptr),
    m_generator(generator)
{
    ui->setupUi(this);
    connect(m_generator, &ArbGenerator::response, this, &FileUploader::readResponse);
}

FileUploader::~FileUploader()
{
}

void FileUploader::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    abort();
}

//------------------------------------------------------

void FileUploader::uploadFile(const QString &path)
{
   if(worker)
       return;

   ui->label->setText(tr("Uploading file %1...").arg(path));
   this->setWindowTitle("Uploading file...");

   worker = new FileUploaderWorker(path);

   connect(worker, &FileUploaderWorker::finished, this, &FileUploader::onFinished);
   connect(worker, &FileUploaderWorker::error, this, &FileUploader::onError);
   connect(worker, &FileUploaderWorker::progress, this, &FileUploader::onProgress);
   connect(worker, &FileUploaderWorker::command, m_generator, &ArbGenerator::sendCommand);
   connect(worker, &FileUploaderWorker::finished, m_generator, &ArbGenerator::uploadFinished);
   connect(this, &FileUploader::abortCommand, m_generator, &ArbGenerator::uploadAborted);

   worker->start();

   this->exec();
}

void FileUploader::readResponse(bool isOK)
{
   if(!worker)
       return;

   if(isOK)
       worker->continueWork();
   else
       worker->repeatCommand();
}

//------------------------------------------------------

void FileUploader::abort()
{
    emit abortCommand();
    if (worker)
        worker->abort();
}

//------------------------------------------------------

void FileUploader::onError(QString message)
{
    qDebug() << "error" << message;

    worker->deleteLater();
    worker = nullptr;

    QMessageBox::critical(this, tr("Error"), message);
    reject();
}

//------------------------------------------------------

void FileUploader::onFinished()
{
    qDebug() << "finished";

    worker->deleteLater();
    worker = nullptr;

    this->disconnect();

    QMessageBox::information(this, tr("Success"), tr("File uploaded successfully"));
    accept();
}

//------------------------------------------------------

void FileUploader::onProgress(int done, int total)
{
    ui->progressBar->setMaximum(total);
    ui->progressBar->setValue(done);
}

//------------------------------------------------------

FileUploaderWorker::FileUploaderWorker(const QString &path) :
    QThread(nullptr),
    m_running(false), m_path(path), m_response_OK(false), m_repeat_command(false)
{
}

//------------------------------------------------------

void FileUploaderWorker::run()
{
   m_running = true;

   emit progress(0, 4096);

   QFile file(m_path);
   if(!file.open(QIODevice::ReadOnly))
   {
       emit error("Unable to open file");
   }

   QTextStream  instream(&file);
   QString value = "";

   int values_to_send[4096];
   bool isOK = false;
   int index = 0;

   while(1)
   {
       if(instream.atEnd()) break;

       if(index > 4095)
       {
           emit error("Too much probes");
           return;
       }

       value = instream.readLine();
       values_to_send[index++] = value.toInt(&isOK);

       if(!isOK)
       {
           emit error(tr("Incorrect value at %d line").arg(index-1));
           return;
       }

       if(values_to_send[index-1] < 0 || values_to_send[index-1] > 4095)
       {
           emit error(tr("Value is out of range at %d line").arg(index-1));
           return;
       }
   }

   if(index != 4096)
   {
       emit error("Not enough probes");
       return;
   }

   m_repeat_command = true;
   m_response_OK = false;

   sendCommand("UPLOAD_ABORT\r");
   sendCommand("STOP_DAC\r");

   int counter = 0;
   int values_amount = 0;
   QString values = "";

   while(1)
   {
       qDebug() << counter;
       if (!m_running)
       {   
           emit error("Operation aborted!");
           return;
       }

       if(counter + 45 < 4095)
           values_amount = counter + 45;
       else
           values_amount = 4096;

       for(int i = counter; i < values_amount; i++)
       {
           values += counter != values_amount - 1 ?
                       QString::number(values_to_send[counter++]) + "," :
                       QString::number(values_to_send[counter++]);
       }

       sendCommand(QString("UPLOAD_SIGNAL " + values + "\r"));

       values = "";
       emit progress(counter+1, 4096);
       if(counter >= 4095) break;
   }

   file.close();

   emit finished();
}

void FileUploaderWorker::sendCommand(QString command_str)
{
    m_repeat_command = true;

    while(m_running)
    {
        if(m_repeat_command)
        {
            emit command(command_str);

            m_repeat_command = false;
            m_response_OK = false;
        }

        if(m_response_OK)
            break;
    }

}

void FileUploaderWorker::continueWork()
{
    m_response_OK = true;
}

void FileUploaderWorker::repeatCommand()
{
    m_repeat_command = true;
}

//------------------------------------------------------

void FileUploaderWorker::abort()
{
   m_running = false;

   qDebug() << "aborting";
}

//------------------------------------------------------
