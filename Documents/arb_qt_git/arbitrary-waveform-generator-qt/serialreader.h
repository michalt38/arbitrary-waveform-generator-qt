#ifndef SERIALREADER_H
#define SERIALREADER_H

#include <QDialog>
#include <QLineEdit>
#include <QCheckBox>

namespace Ui {
class SerialReader;
}

class ArbGenerator;

class SerialReader : public QDialog
{
    Q_OBJECT

public:
    explicit SerialReader(ArbGenerator &generator, QWidget *parent = 0);
    ~SerialReader();
    void closeEvent(QCloseEvent *event);
    void startReading();
    void stopReading();

public slots:
    void addCR();
    void sendCommand();
    void appendSerialText(QString text);

signals:
    void command(QString command);

private:
    ArbGenerator &m_generator;
    bool m_addCR;
    Ui::SerialReader *ui;
    QLineEdit *commandToSend;
    QCheckBox *CR;
    QPushButton *sendButton;
};

#endif // SERIALREADER_H
