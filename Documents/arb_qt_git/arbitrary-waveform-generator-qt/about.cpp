#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    connect(ui->closePushButton, &QPushButton::clicked, this, &About::close);
    this->setWindowTitle("About");
}

About::~About()
{
    delete ui;
}
