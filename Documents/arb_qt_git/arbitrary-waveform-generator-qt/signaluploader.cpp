#include "SignalUploader.h"
#include "ui_SignalUploader.h"

#include <QFileInfo>
#include <QThread>
#include <QDebug>
#include <QMutex>
#include <QMessageBox>
#include "arbgenerator.h"

SignalUploader::SignalUploader(ArbGenerator *generator, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SignalUploader),
    worker(nullptr),
    m_generator(generator){
    ui->setupUi(this);
    connect(m_generator, &ArbGenerator::response, this, &SignalUploader::readResponse);
}

SignalUploader::~SignalUploader()
{
}

void SignalUploader::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    abort();
}

//------------------------------------------------------

void SignalUploader::uploadSignal(QVector<double> &values)
{
   if(worker)
       return;

   this->setWindowTitle("Uploading signal...");

   worker = new SignalUploaderWorker(values);

   connect(worker, &SignalUploaderWorker::finished, this, &SignalUploader::onFinished);
   connect(worker, &SignalUploaderWorker::error, this, &SignalUploader::onError);
   connect(worker, &SignalUploaderWorker::progress, this, &SignalUploader::onProgress);
   connect(worker, &SignalUploaderWorker::command, m_generator, &ArbGenerator::sendCommand);
   connect(worker, &SignalUploaderWorker::finished, m_generator, &ArbGenerator::uploadFinished);
   connect(this, &SignalUploader::abortCommand, m_generator, &ArbGenerator::uploadAborted);

   worker->start();

   this->exec();
}

void SignalUploader::readResponse(bool isOK)
{
   if(!worker)
       return;

   if(isOK)
       worker->continueWork();
   else
       worker->repeatCommand();
}

//------------------------------------------------------

void SignalUploader::abort()
{
    emit abortCommand();

    if (worker)
        worker->abort();
}

//------------------------------------------------------

void SignalUploader::onError(QString message)
{
    qDebug() << "error" << message;

    worker->deleteLater();
    worker = nullptr;

    QMessageBox::critical(this, tr("Error"), message);
    reject();
}

//------------------------------------------------------

void SignalUploader::onFinished()
{
    qDebug() << "finished";

    worker->deleteLater();
    worker = nullptr;

    this->disconnect();

    QMessageBox::information(this, tr("Success"), tr("Signal uploaded successfully"));
    accept();
}

//------------------------------------------------------

void SignalUploader::onProgress(int done, int total)
{
    ui->progressBar->setMaximum(total);
    ui->progressBar->setValue(done);
}

//------------------------------------------------------

SignalUploaderWorker::SignalUploaderWorker(QVector<double> &values) :
    QThread(nullptr),
    m_running(false), m_values(values)
{
}

//------------------------------------------------------

void SignalUploaderWorker::run()
{
   m_running = true;

   emit progress(0, 4096);

   sendCommand("UPLOAD_ABORT\r");
   sendCommand("STOP_DAC\r");

   int values_amount = 0;
   QString values_str = "";

   for(int counter = 0; counter < 4096; )
   {
       qDebug() << counter;
       if (!m_running)
       {
           emit error("Operation aborted!");
           return;
       }

       if(counter + 45 < 4095)
           values_amount = counter + 45;
       else
           values_amount = 4096;

       for(int i = counter; i < values_amount; i++)
       {
           values_str += counter != values_amount - 1 ?
                       QString::number(m_values[counter++]) + "," :
                       QString::number(m_values[counter++]);
       }

       sendCommand(QString("UPLOAD_SIGNAL " + values_str + "\r"));

       values_str = "";
       emit progress(counter+1, 4096);
       if(counter >= 4095) break;
   }

   emit finished();
}

void SignalUploaderWorker::sendCommand(QString command_str)
{
    m_repeat_command = true;

    while(m_running)
    {
        if(m_repeat_command)
        {
            emit command(command_str);

            m_repeat_command = false;
            m_response_OK = false;
        }

        if(m_response_OK)
            break;
    }

}

void SignalUploaderWorker::continueWork()
{
    m_response_OK = true;
}

void SignalUploaderWorker::repeatCommand()
{
    m_repeat_command = true;
}

//------------------------------------------------------

void SignalUploaderWorker::abort()
{
   m_running = false;

   qDebug() << "aborting";
}

//------------------------------------------------------
