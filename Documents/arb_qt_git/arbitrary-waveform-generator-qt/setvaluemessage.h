#ifndef SETVALUEMESSAGE_H
#define SETVALUEMESSAGE_H

#include <QDialog>

namespace Ui {
class SetValueMessage;
}

class SetValueMessage : public QDialog
{
    Q_OBJECT

public:
    explicit SetValueMessage(QString title, QString text, int minValue, int maxValue, QWidget *parent = 0);
    ~SetValueMessage();
    int getValue();

private slots:
    void setValue(int value);

private:
    Ui::SetValueMessage *ui;
    int m_value;
};

#endif // SETVALUEMESSAGE_H
