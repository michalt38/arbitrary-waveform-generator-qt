#ifndef SIGNALEDITOR_H
#define SIGNALEDITOR_H

#include <QDialog>
#include <QStatusBar>
#include "qcustomplot.h"
#include "signaluploader.h"

class ArbGenerator;

namespace Ui {
class SignalEditor;
}

class SignalEditor : public QDialog
{
    Q_OBJECT

public:
    explicit SignalEditor(ArbGenerator *generator, QWidget *parent = 0);
    ~SignalEditor();

public slots:
    void mouseMoved(QMouseEvent *event);
    void mouseClicked(QMouseEvent *event);
    void mouseDoubleClicked(QMouseEvent *event);
    void mouseReleased(QMouseEvent *event);
    void removePoints();
    void saveSignal();
    void loadSignal();
    void setScaledProbesNum(int probes);
    void plotSelected();
    void makeSignalPeriodic();
    void resetSignal();
    void maximizeSignal();
    void uploadSignal();

private:
    Ui::SignalEditor *ui;
    int m_dataIndex;
    bool m_plotClicked;
    int m_scaledProbesNum;
    bool m_nodesSelected;
    QPushButton *saveButton;
    QPushButton *loadButton;
    QPushButton *closeButton;
    QPushButton *periodityButton;
    QPushButton *resetButton;
    QPushButton *maximizeButton;
    QPushButton *uploadButton;
    QSpinBox *scaledProbesSpinBox;
    QStatusBar *statusBar;
    SignalUploader *signaluploader;
    QVector<double> x, y, y_points, x_nodes, y_nodes;
};

#endif // SIGNALEDITOR_H
