#-------------------------------------------------
#
# Project created by QtCreator 2016-09-08T10:48:49
#
#-------------------------------------------------

QT       += core gui serialport

QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Arbitrary-waveform-generator
TEMPLATE = app


SOURCES += main.cpp\
        arbgenerator.cpp \
    serialdialog.cpp \
    fileuploader.cpp \
    about.cpp \
    serialreader.cpp \
    qcustomplot.cpp \
    signaleditor.cpp \
    setvaluemessage.cpp \
    signaluploader.cpp

HEADERS  += arbgenerator.h \
    serialdialog.h \
    fileuploader.h \
    about.h \
    serialreader.h \
    qcustomplot.h \
    signaleditor.h \
    setvaluemessage.h \
    signaluploader.h

FORMS    += arbgenerator.ui \
    serialdialog.ui \
    fileuploader.ui \
    about.ui \
    serialreader.ui \
    signaleditor.ui \
    setvaluemessage.ui \
    signaluploader.ui

RESOURCES += \
    agh_logo.qrc \
    icon.qrc
