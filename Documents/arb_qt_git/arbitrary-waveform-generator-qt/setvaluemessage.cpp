#include "setvaluemessage.h"
#include "ui_setvaluemessage.h"

SetValueMessage::SetValueMessage(QString title, QString text, int minValue, int maxValue, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetValueMessage)
{
    ui->setupUi(this);
    this->setWindowTitle(title);
    ui->message->setText(text);
    ui->valueBox->setMinimum(minValue);
    ui->valueBox->setMaximum(maxValue);

    connect(ui->valueBox, SIGNAL(valueChanged(int)), this, SLOT(setValue(int)));
    m_value = 0;
}

SetValueMessage::~SetValueMessage()
{
    delete ui;
}

void SetValueMessage::setValue(int value)
{
    m_value = value;
}

int SetValueMessage::getValue()
{
    return m_value;
}
