#ifndef FILEUPLOADER_H
#define FILEUPLOADER_H

#include <QDialog>
#include <QFile>
#include <QSerialPort>
#include <QMutex>
#include <QThread>

class ArbGenerator;

namespace Ui
{
class FileUploader;
}

class FileUploaderWorker : public QThread
{
    Q_OBJECT

public:
    FileUploaderWorker(const QString &path);
    void sendCommand(QString command_str);
    void continueWork();
    void repeatCommand();
    void run();

public slots:
    void abort();

signals:
    void finished();
    void error(QString message);
    void progress(int done, int total);
    void command(QString command);

private:
    bool m_running;
    QString m_path;
    bool m_response_OK;
    bool m_repeat_command;
};

class FileUploader : public QDialog
{
    Q_OBJECT

public:
    explicit FileUploader(ArbGenerator *generator, QWidget *parent = nullptr);
    ~FileUploader();
    void uploadFile(const QString &path);

public slots:
    void readResponse(bool isOK);
    void closeEvent(QCloseEvent *event);
    void abort();

private slots:
    void onError(QString message);
    void onFinished();
    void onProgress(int done, int total);

signals:
    void abortCommand();

private:
    Ui::FileUploader *ui;
    FileUploaderWorker *worker;
    ArbGenerator *m_generator;
};

#endif // FILEUPLOADER_H
