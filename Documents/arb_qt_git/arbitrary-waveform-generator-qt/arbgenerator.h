#ifndef ARBGENERATOR_H
#define ARBGENERATOR_H

#include <QMainWindow>
#include <QSerialPort>
#include "serialdialog.h"
#include "fileuploader.h"
#include <QStringListModel>
#include <QModelIndex>
#include "serialreader.h"
#include "signaleditor.h"

namespace Ui {
class ArbGenerator;
}

typedef enum lastAction {FREQ, INCR, PSC, NONE} lastAction;

class ArbGenerator : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArbGenerator(QWidget *parent = 0);
    void freezeUi();
    void releaseUi();
    void setSignalSettingsToDefault();
    void closeEvent(QCloseEvent *event);
    ~ArbGenerator();

public slots:
    void readSerial();
    void setFrequency(int freq);
    void setAmplitude(double ampl);
    void setIncrementation(int incrementation);
    void setSamplingRate(double rate);
    void setSignal(QModelIndex);
    void setSerialPort();
    void uploadFile();
    void sendCommand(QString command);
    void reverseSignal();
    void setDutyCycle(int cycle);
    void setExpectedValue(double value);
    void setVariance(double var);
    void setSignalType(QString signal_type);
    void setFactor(double factor);
    void uploadAborted();
    void uploadFinished();
    void about();
    void openSerialReader();
    void openSignalEditor();

signals:
    void command(QString command);
    void response(bool isOK);
    void responseText(QString responseText);

private:
    Ui::ArbGenerator *ui;
    QSerialPort *serial;
    SerialDialog *serialdialog;
    QStringList signalList;
    QStringListModel *model;
    QString responseBuffer;
    lastAction m_lastAction;
    QString command_to_send;
    SerialReader *serialreader;
    FileUploader *uploader;
    SignalEditor *signaleditor;
    bool get_status;
};

#endif // ARBGENERATOR_H
