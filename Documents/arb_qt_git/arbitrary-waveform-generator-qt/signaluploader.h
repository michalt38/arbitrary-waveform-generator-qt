#ifndef SIGNALUPLOADER_H
#define SIGNALUPLOADER_H

#include <QDialog>
#include <QFile>
#include <QSerialPort>
#include <QMutex>
#include <QThread>

class ArbGenerator;

namespace Ui
{
class SignalUploader;
}

class SignalUploaderWorker : public QThread
{
    Q_OBJECT

public:
    SignalUploaderWorker(QVector<double> &values);
    void sendCommand(QString command_str);
    void continueWork();
    void repeatCommand();
    void run();

public slots:
    void abort();

signals:
    void finished();
    void error(QString message);
    void progress(int done, int total);
    void command(QString command);

private:
    bool m_running;
    QVector<double> m_values;
    bool m_response_OK;
    bool m_repeat_command;
};

class SignalUploader : public QDialog
{
    Q_OBJECT

public:
    explicit SignalUploader(ArbGenerator *generator, QWidget *parent = nullptr);
    ~SignalUploader();
    void uploadSignal(QVector<double> &values);

public slots:
    void readResponse(bool isOK);
    void closeEvent(QCloseEvent *event);
    void abort();

private slots:
    void onError(QString message);
    void onFinished();
    void onProgress(int done, int total);

signals:
    void abortCommand();

private:
    Ui::SignalUploader *ui;
    SignalUploaderWorker *worker;
    ArbGenerator *m_generator;
};

#endif // SIGNALUPLOADER_H
