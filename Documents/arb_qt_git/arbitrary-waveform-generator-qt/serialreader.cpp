#include "serialreader.h"
#include "ui_serialreader.h"
#include "arbgenerator.h"

SerialReader::SerialReader(ArbGenerator &generator, QWidget *parent) :
    QDialog(parent),
    m_generator(generator),
    ui(new Ui::SerialReader)
{
    ui->setupUi(this);
    ui->serialText->setReadOnly(true);
    this->setWindowTitle("Serial reader");
    m_addCR = false;

    commandToSend = ui->serialWriterWidget->findChild<QLineEdit*>("commandLine");
    CR = ui->serialWriterWidget->findChild<QCheckBox*>("CRcheckBox");
    sendButton = ui->serialWriterWidget->findChild<QPushButton*>("sendPushButton");

    connect(this, &SerialReader::command, &generator, &ArbGenerator::sendCommand);
    connect(CR, &QCheckBox::toggled, this, &SerialReader::addCR);
    connect(sendButton, &QPushButton::clicked, this, &SerialReader::sendCommand);
}

void SerialReader::startReading()
{
    connect(&m_generator, &ArbGenerator::responseText, this, &SerialReader::appendSerialText);
}

void SerialReader::sendCommand()
{
    QString command_string = "";
    command_string = commandToSend->text();
    commandToSend->clear();
    if(m_addCR) command_string += "\r";
    emit command(command_string);
}

void SerialReader::appendSerialText(QString text)
{
    if(text.contains("\r\n"))
        text.remove("\r\n");

    ui->serialText->append(text);
}

void SerialReader::addCR()
{
    m_addCR = !m_addCR;
}

void SerialReader::stopReading()
{
    ui->serialText->clear();
    disconnect(&m_generator, &ArbGenerator::responseText, this, &SerialReader::appendSerialText);
}

void SerialReader::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    this->stopReading();
}

SerialReader::~SerialReader()
{
    delete commandToSend;
    delete CR;
    delete sendButton;
    delete ui;
}
